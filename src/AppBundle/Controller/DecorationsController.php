<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Decorations;
use AppBundle\Controller\DecorationsController;

/**
 * Decorations table controller.
 *
 * @Route("decorations")
 */
class DecorationsController extends Controller
{
    /**
     * View All - Read Only.
     *
     * @Route("/read", name="decorations_read")
     * @Method("GET")
     */
    public function readAction()
    {
      $em = $this->getDoctrine()->getManager();

      $post = $this->getDoctrine()->getRepository('AppBundle:Decorations')->findAll();

      return $this->render('decorations/read.html.twig',[
        'appdata' => $post
      ]);
    }

    /**
     * View All.
     *
     * @Route("/index", name="decorations_index")
     * @Method("GET")
     */
    public function viewAction()
    {
      $em = $this->getDoctrine()->getManager();

      $post = $this->getDoctrine()->getRepository('AppBundle:Decorations')->findAll();

      return $this->render('decorations/index.html.twig',[
        'appdata' => $post
      ]);
    }

    /**
     * Select Entity Value Page.
     *
     * @Route("/view_all", name="decorations_select")
     * @Method("GET")
     */
    public function selectAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();

      if ($request->query->get('selectbox1')) {
        return $this->redirectToRoute('decorations_edit', array('decoration' => $request->query->get('selectbox1')));
      }

      $post = $this->getDoctrine()->getRepository('AppBundle:Decorations')->findAll();

      return $this->render('decorations/select.html.twig',[
        'appdata' => $post
      ]);
    }

    /**
     * Finds and displays entity value to edit.
     *
     * @Route("/edit/{decoration}", name="decorations_edit", requirements={"decoration"=".+"})
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request, Decorations $val)
    {
      $editForm = $this->createForm('AppBundle\Form\DecorationsUpdateType', $val);
      $editForm->handleRequest($request);
      $deleteForm = $this->createDeleteForm($val);

      if ($editForm->isSubmitted() && $editForm->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUsername();
        $val->setUsermodi($user);
        $em->flush();

        $this->addFlash('info', 'Entry Updated!');
      }

      $post = $this->getDoctrine()->getRepository('AppBundle:Decorations')->findAll();

      return $this->render('decorations/edit.html.twig', [
          'appdata2' => $post,
          'appdata' => $val,
          'edit_form' => $editForm->createView(),
          'delete_form' => $deleteForm->createView(),
      ]);
    }

    /**
     * Creates a new entity.
     *
     * @Route("/new", name="decorations_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $val = new Decorations();
        $form = $this->createForm('AppBundle\Form\DecorationsCreateType', $val);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->container->get('security.token_storage')->getToken()->getUsername();
            $em->persist($val);
            $val->setUsercrea($user);
            $em->flush();

            $this->addFlash('success', 'New Entry Created!');

            return $this->redirectToRoute('decorations_edit', array('decoration' => $val->getDecoration()));
        }

        return $this->render('decorations/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes an entity.
     *
     * @Route("/delete/{decoration}", name="decorations_delete", requirements={"decoration"=".+"})
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Decorations $val)
    {
        $form = $this->createDeleteForm($val);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($val);
            $em->flush();

            $this->addFlash('danger', 'Delete Successful!');
        }

        return $this->redirectToRoute('decorations_index');
    }

    /**
     * Creates a form to delete an entity.
     *
     * @param Decorations $val The Form entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Decorations $val)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('decorations_delete', array('decoration' => $val->getDecoration())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

}
