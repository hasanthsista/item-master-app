<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Levels;

/**
 * Levels table controller.
 *
 * @Route("levels")
 */
class LevelsController extends Controller
{
    /**
     * View All.
     *
     * @Route("/index", name="levels_index")
     * @Method("GET")
     */
    public function viewAction()
    {
      $em = $this->getDoctrine()->getManager();

      $post = $this->getDoctrine()->getRepository('AppBundle:Levels')->findAll();

      return $this->render('levels/index.html.twig',[
        'appdata' => $post
      ]);
    }

    /**
     * Select Entity Value Page.
     *
     * @Route("/view_all", name="levels_select")
     * @Method("GET")
     */
    public function selectAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();

      if ($request->query->get('selectbox1')) {
        return $this->redirectToRoute('levels_edit', array('srLevel' => $request->query->get('selectbox1')));
      }

      $post = $this->getDoctrine()->getRepository('AppBundle:Levels')->findAll();

      return $this->render('levels/select.html.twig',[
        'appdata' => $post
      ]);
    }


        /**
         * Finds and displays entity value to edit.
         *
         * @Route("/edit/{srLevel}", name="levels_edit", requirements={"srLevel"=".+"})
         * @Method({"GET", "POST"})
         */
        public function showAction(Request $request, Levels $val)
        {
          $editForm = $this->createForm('AppBundle\Form\LevelsUpdateType', $val);
          $editForm->handleRequest($request);
           $deleteForm = $this->createDeleteForm($val);

          if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->container->get('security.token_storage')->getToken()->getUsername();
            $val->setUsermodi($user);
            $em->flush();

            $this->addFlash('info', 'Entry Updated!');
          }

          $post = $this->getDoctrine()->getRepository('AppBundle:Levels')->findAll();

          return $this->render('levels/edit.html.twig', [
              'appdata2' => $post,
              'appdata' => $val,
              'edit_form' => $editForm->createView(),
               'delete_form' => $deleteForm->createView(),
          ]);
        }

        /**
         * Creates a new entity.
         *
         * @Route("/new", name="levels_new")
         * @Method({"GET", "POST"})
         */
        public function newAction(Request $request)
        {
            $val = new Levels();
            $form = $this->createForm('AppBundle\Form\LevelsCreateType', $val);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $user = $this->container->get('security.token_storage')->getToken()->getUsername();
                $em->persist($val);
                $val->setUsercrea($user);
                $em->flush();

                $this->addFlash('success', 'New Entry Created!');

                return $this->redirectToRoute('levels_edit', array('srLevel' => $val->getsrLevel()));
            }

            return $this->render('levels/new.html.twig', [
                'form' => $form->createView(),
            ]);
        }


            /**
             * Deletes an entity.
             *
             * @Route("/delete/{srLevel}", name="levels_delete", requirements={"srLevel"=".+"})
             * @Method("DELETE")
             */
            public function deleteAction(Request $request, Levels $val)
            {
                $form = $this->createDeleteForm($val);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($val);
                    $em->flush();

                    $this->addFlash('danger', 'Delete Successful!');
                }

                return $this->redirectToRoute('levels_index');
            }

            /**
             * Creates a form to delete an entity.
             *
             * @param Levels $val The Form entity
             *
             * @return \Symfony\Component\Form\Form The form
             */
            private function createDeleteForm(Levels $val)
            {
                return $this->createFormBuilder()
                    ->setAction($this->generateUrl('levels_delete', array('srLevel' => $val->getsrLevel())))
                    ->setMethod('DELETE')
                    ->getForm()
                ;
            }
            }
