<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Color;

/**
 * colors table controller.
 *
 * @Route("Color")
 */

 class ColorController extends Controller
 {
     /**
      * View All.
      *
      * @Route("/index", name="colors_index")
      * @Method("GET")
      */

     public function viewAction()
     {
         $em = $this->getDoctrine()->getManager();

         $post = $this->getDoctrine()->getRepository('AppBundle:Color')->findAll();

         return $this->render('colors/index.html.twig', [
        'appdata' => $post
      ]);
     }

     /**
      * Select Entity Value Page.
      *
      * @Route("/view_all", name="colors_select")
      * @Method("GET")
      */

     public function selectAction(Request $request)
     {
         $em = $this->getDoctrine()->getManager();

         if ($request->query->get('selectbox1')) {
             return $this->redirectToRoute('colors_edit', array('colorcode' => $request->query->get('selectbox1')));
         }

         $post = $this->getDoctrine()->getRepository('AppBundle:Color')->findAll();

         return $this->render('colors/select.html.twig', [
        'appdata' => $post
      ]);
     }


     /**
            * Finds and displays entity value to edit.
            *
            * @Route("/edit/{colorcode}", name="colors_edit", requirements={"colorcode"=".+"})
            * @Method({"GET", "POST"})
            */

     public function showAction(Request $request, Color $val)
     {
         $editForm = $this->createForm('AppBundle\Form\ColorUpdateType', $val);
         $editForm->handleRequest($request);
         $deleteForm = $this->createDeleteForm($val);

         if ($editForm->isSubmitted() && $editForm->isValid()) {
             $em = $this->getDoctrine()->getManager();
             $em->persist($editForm->getData());
             $em->flush();
             $this->addFlash('info', 'Entry Updated!');
         }


         $post = $this->getDoctrine()->getRepository('AppBundle:Color')->findAll();

         return $this->render('colors/edit.html.twig', [
              'appdata2' => $post,
              'appdata' => $val,
              'edit_form' => $editForm->createView(),
               'delete_form' => $deleteForm->createView(),
          ]);
     }

     /**
      * Creates a new entity.
      *l=
      * @Route("/new", name="colors_new")
      * @Method({"GET", "POST"})
      */

     public function newAction(Request $request)
     {
         $val = new Color();
         $form = $this->createForm('AppBundle\Form\ColorCreateType', $val);
         $form->handleRequest($request);

         if ($form->isSubmitted() && $form->isValid()) {
             $em = $this->getDoctrine()->getManager();
             $user = $this->container->get('security.token_storage')->getToken()->getUsername();
             $em->persist($val);

             $em->flush();

             $this->addFlash('success', 'New Entry Created!');

             return $this->redirectToRoute('colors_edit', array('colorcode' => $val->getColorcode()));
         }

         return $this->render('colors/new.html.twig', [
                'form' => $form->createView(),
            ]);
     }

     /**
        * Deletes an entity.
        *
        * @Route("/delete/{colorcode}", name="colors_delete", requirements={"colorcode"=".+"})
        * @Method("DELETE")
        */

     public function deleteAction(Request $request, Color $val)
     {
         $form = $this->createDeleteForm($val);
         $form->handleRequest($request);

         if ($form->isSubmitted() && $form->isValid()) {
             $em = $this->getDoctrine()->getManager();
             $em->remove($val);
             $em->flush();

             $this->addFlash('danger', 'Delete Successful!');
         }

         return $this->redirectToRoute('colors_index');
     }

     /**
      * Creates a form to delete an entity.
      *
      * @param Color $val The Form entity
      *
      * @return \Symfony\Component\Form\Form The form
      */
     private function createDeleteForm(Color $val)
     {
         return $this->createFormBuilder()
                    ->setAction($this->generateUrl('colors_delete', array('colorcode' => $val->getColorcode())))
                    ->setMethod('DELETE')
                    ->getForm()

                    ;
     }
 }
