<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\PackType;
use AppBundle\Entity\Processes;

/**
 * Levels table controller.
 *
 * @Route("Processes")
 */
class ProcessesController extends Controller
{
    /**
     * View All.
     *
     * @Route("/index", name="processes_index")
     * @Method("GET")
     */
    public function viewAction()
    {
      $em = $this->getDoctrine()->getManager();

      $post = $this->getDoctrine()->getRepository('AppBundle:Processes')->findAll();

      return $this->render('processes/index.html.twig',[
        'appdata' => $post
      ]);
    }


    /**
     * Creates a new entity.
     *
     * @Route("/new", name="processes_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $val = new Processes();
        $form = $this->createForm('AppBundle\Form\ProcessesCreateType', $val);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->container->get('security.token_storage')->getToken()->getUsername();
            $em->persist($val);
            $val->setUsercrea($user);
            $val->setUsermodi($user);
            $em->flush();

            $this->addFlash('success', 'New Entry Created!');

            return $this->redirectToRoute('processes_edit', array('manProcess' => $val->getManProcess()));
        }

        return $this->render('processes/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }


        /**
         * Select Entity Value Page.
         *
         * @Route("/view_all", name="processes_select")
         * @Method("GET")
         */
        public function selectAction(Request $request)
        {

          $em = $this->getDoctrine()->getManager();
          if ($request->query->get('selectbox1')!="") {

            return $this->redirectToRoute('processes_edit', array('manProcess' => $request->query->get('selectbox1')));
          }

          $post = $this->getDoctrine()->getRepository('AppBundle:Processes')->findAll();
          // dump($post); die;

          return $this->render('processes/select.html.twig',[
            'appdata' => $post
          ]);
        }


            /**
             * Finds and displays entity value to edit.
             *
             * @Route("/edit/{manProcess}", name="processes_edit", requirements={"manProcess"=".+"})
             * @Method({"GET", "POST"})
             */
            public function showAction(Request $request, Processes $val)
            {
              $editForm = $this->createForm('AppBundle\Form\ProcessesUpdateType', $val);
              $editForm->handleRequest($request);
              $deleteForm = $this->createDeleteForm($val);

              if ($editForm->isSubmitted() && $editForm->isValid()) {


                $em = $this->getDoctrine()->getManager();

                $editForm->getData()->updateModifiedDatetime();

                $em->persist($editForm->getData());
                $em->flush();

                $this->addFlash('info', 'Entry Updated!');
              }

              $post = $this->getDoctrine()->getRepository('AppBundle:Processes')->findAll();

              return $this->render('processes/edit.html.twig', [
                  'appdata2' => $post,
                  'appdata' => $val,
                  'edit_form' => $editForm->createView(),
                  'delete_form' => $deleteForm->createView()
              ]);
            }



            /**
             * Deletes an entity.
             *
             * @Route("/delete/{manProcess}", name="processes_delete", requirements={"manProcess"=".+"})
             * @Method("DELETE")
             */
            public function deleteAction(Request $request, Processes $val)
            {
                $form = $this->createDeleteForm($val);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($val);
                    $em->flush();

                    $this->addFlash('danger', 'Delete Successful!');
                }

                return $this->redirectToRoute('processes_index');
            }

            /**
             * Creates a form to delete an entity.
             *
             * @param Processes $val The Form entity
             *
             * @return \Symfony\Component\Form\Form The form
             */
            private function createDeleteForm(Processes $val)
            {
                return $this->createFormBuilder()
                    ->setAction($this->generateUrl('processes_delete', array('manProcess' => $val->getManProcess())))
                    ->setMethod('DELETE')
                    ->getForm()
                ;
            }



          }
