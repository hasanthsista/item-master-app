<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Classes;

/**
 * classes table controller.
 *
 * @Route("Classes")
 */

 class ClassesController extends Controller
 {
     /**
      * View All.
      *
      * @Route("/index", name="classes_index")
      * @Method("GET")
      */

     public function viewAction()
     {
         $em = $this->getDoctrine()->getManager();

         $post = $this->getDoctrine()->getRepository('AppBundle:Classes')->findAll();

         return $this->render('classes/index.html.twig', [
        'appdata' => $post
      ]);
     }

     /**
      * Select Entity Value Page.
      *
      * @Route("/view_all", name="classes_select")
      * @Method("GET")
      */

     public function selectAction(Request $request)
     {
         $em = $this->getDoctrine()->getManager();

         if ($request->query->get('selectbox1')) {
             return $this->redirectToRoute('classes_edit', array('class' => $request->query->get('selectbox1')));
         }

         $post = $this->getDoctrine()->getRepository('AppBundle:Classes')->findAll();

         return $this->render('classes/select.html.twig', [
        'appdata' => $post
      ]);
     }

     /**
            * Finds and displays entity value to edit.
            *
            * @Route("/edit/{class}", name="classes_edit", requirements={"class"=".+"})
            * @Method({"GET", "POST"})
            */

     public function showAction(Request $request, Classes $val)
     {
         $editForm = $this->createForm('AppBundle\Form\ClassesUpdateType', $val);
         $editForm->handleRequest($request);
         $deleteForm = $this->createDeleteForm($val);

         if ($editForm->isSubmitted() && $editForm->isValid()) {
             $em = $this->getDoctrine()->getManager();
             $em->persist($editForm->getData());
             $em->flush();
             $this->addFlash('info', 'Entry Updated!');
         }


         $post = $this->getDoctrine()->getRepository('AppBundle:Classes')->findAll();

         return $this->render('classes/edit.html.twig', [
               'appdata2' => $post,
               'appdata' => $val,
               'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
           ]);
     }

     /**
      * Creates a new entity.
      *l=
      * @Route("/new", name="classes_new")
      * @Method({"GET", "POST"})
      */

     public function newAction(Request $request)
     {
         $val = new Classes();
         $form = $this->createForm('AppBundle\Form\ClassesCreateType', $val);
         $form->handleRequest($request);

         if ($form->isSubmitted() && $form->isValid()) {
             $em = $this->getDoctrine()->getManager();
             $user = $this->container->get('security.token_storage')->getToken()->getUsername();
             $em->persist($val);

             $em->flush();

             $this->addFlash('success', 'New Entry Created!');

             return $this->redirectToRoute('classes_edit', array('class' => $val->getClass()));
         }

         return $this->render('classes/new.html.twig', [
                 'form' => $form->createView(),
             ]);
     }

     /**
        * Deletes an entity.
        *
        * @Route("/delete/{class}", name="classes_delete", requirements={"class"=".+"})
        * @Method("DELETE")
        */

     public function deleteAction(Request $request, Classes $val)
     {
         $form = $this->createDeleteForm($val);
         $form->handleRequest($request);

         if ($form->isSubmitted() && $form->isValid()) {
             $em = $this->getDoctrine()->getManager();
             $em->remove($val);
             $em->flush();

             $this->addFlash('danger', 'Delete Successful!');
         }

         return $this->redirectToRoute('classes_index');
     }

     /**
      * Creates a form to delete an entity.
      *
      * @param Classes $val The Form entity
      *
      * @return \Symfony\Component\Form\Form The form
      */
     private function createDeleteForm(Classes $val)
     {
         return $this->createFormBuilder()
                     ->setAction($this->generateUrl('classes_delete', array('class' => $val->getClass())))
                     ->setMethod('DELETE')
                     ->getForm()

                     ;
     }
 }
