<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Decorations table controller.
 *
 * @Route("home")
 */
class HomeController extends Controller
{
    /**
     * View All - Read Only.
     *
     * @Route("/", name="im_home")
     * @Method("GET")
     */
    public function homeAction()
    {
      return $this->render('home/home.html.twig');
    }
}
