<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\UpcCode;

/**
 * Levels table controller.
 *
 * @Route("UpcCode")
 */
class UpcCodeController extends Controller
{
    /**
     * View All.
     *
     * @Route("/index", name="upccode_index")
     * @Method("GET")
     */
    public function viewAction()
    {
      $em = $this->getDoctrine()->getManager();

      $post = $this->getDoctrine()->getRepository('AppBundle:UpcCode')->findAll();

      return $this->render('upccode/index.html.twig',[
        'appdata' => $post
      ]);
    }


    /**
     * Creates a new entity.
     *
     * @Route("/new", name="upccode_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $val = new UpcCode();
        $form = $this->createForm('AppBundle\Form\UpccodeCreateType', $val);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->container->get('security.token_storage')->getToken()->getUsername();
            $em->persist($val);
            $val->setUsercrea($user);
            $val->setUsermodi($user);
            $em->flush();

            $this->addFlash('success', 'New Entry Created!');

            return $this->redirectToRoute('upccode_edit', array('upc' => $val->getUpc()));
        }

        return $this->render('upccode/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }


        /**
         * Select Entity Value Page.
         *
         * @Route("/view_all", name="upccode_select")
         * @Method("GET")
         */
        public function selectAction(Request $request)
        {

          $em = $this->getDoctrine()->getManager();
          if ($request->query->get('selectbox1')!="") {

            return $this->redirectToRoute('upccode_edit', array('upc' => $request->query->get('selectbox1')));
          }

          $post = $this->getDoctrine()->getRepository('AppBundle:UpcCode')->findAll();
          // dump($post); die;

          return $this->render('upccode/select.html.twig',[
            'appdata' => $post
          ]);
        }


            /**
             * Finds and displays entity value to edit.
             *
             * @Route("/edit/{upc}", name="upccode_edit", requirements={"upc"=".+"})
             * @Method({"GET", "POST"})
             */
            public function showAction(Request $request, UpcCode $val)
            {
              $editForm = $this->createForm('AppBundle\Form\UpccodeUpdateType', $val);
              $editForm->handleRequest($request);
              $deleteForm = $this->createDeleteForm($val);

              if ($editForm->isSubmitted() && $editForm->isValid()) {


                $em = $this->getDoctrine()->getManager();

                $editForm->getData()->updateModifiedDatetime();

                $em->persist($editForm->getData());
                $em->flush();

                $this->addFlash('info', 'Entry Updated!');
              }

              $post = $this->getDoctrine()->getRepository('AppBundle:UpcCode')->findAll();

              return $this->render('upccode/edit.html.twig', [
                  'appdata2' => $post,
                  'appdata' => $val,
                  'edit_form' => $editForm->createView(),
                  'delete_form' => $deleteForm->createView()
              ]);
            }



            /**
             * Deletes an entity.
             *
             * @Route("/delete/{upc}", name="upccode_delete", requirements={"upc"=".+"})
             * @Method("DELETE")
             */
            public function deleteAction(Request $request, UpcCode $val)
            {
                $form = $this->createDeleteForm($val);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($val);
                    $em->flush();

                    $this->addFlash('danger', 'Delete Successful!');
                }

                return $this->redirectToRoute('upccode_index');
            }

            /**
             * Creates a form to delete an entity.
             *
             * @param UpcCode $val The Form entity
             *
             * @return \Symfony\Component\Form\Form The form
             */
            private function createDeleteForm(UpcCode $val)
            {
                return $this->createFormBuilder()
                    ->setAction($this->generateUrl('upccode_delete', array('upc' => $val->getUpc())))
                    ->setMethod('DELETE')
                    ->getForm()
                ;
            }



          }
