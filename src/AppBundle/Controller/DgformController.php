<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Dgform;

/**
 * Dgform table controller.
 *
 * @Route("dgform")
 */
class DgformController extends Controller
{
    /**
     * View All - Read Only.
     *
     * @Route("/read", name="dgform_read")
     * @Method("GET")
     */
    public function readAction()
    {
      $em = $this->getDoctrine()->getManager();

      $post = $this->getDoctrine()->getRepository('AppBundle:Dgform')->findAll();

      return $this->render('dgform/read.html.twig',[
        'appdata' => $post
      ]);
    }

    /**
     * View All.
     *
     * @Route("/index", name="dgform_index")
     * @Method("GET")
     */
    public function viewAction()
    {
      $em = $this->getDoctrine()->getManager();

      $post = $this->getDoctrine()->getRepository('AppBundle:Dgform')->findAll();

      return $this->render('dgform/index.html.twig',[
        'appdata' => $post
      ]);
    }

    /**
     * Select Entity Value Page.
     *
     * @Route("/view_all", name="dgform_select")
     * @Method("GET")
     */
    public function selectAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();

      if ($request->query->get('selectbox1')) {
        return $this->redirectToRoute('dgform_edit', array('formdg' => $request->query->get('selectbox1')));
      }

      $post = $this->getDoctrine()->getRepository('AppBundle:Dgform')->findAll();

      return $this->render('dgform/select.html.twig',[
        'appdata' => $post
      ]);
    }

    /**
     * Finds and displays entity value to edit.
     *
     * @Route("/edit/{formdg}", name="dgform_edit", requirements={"formdg"=".+"})
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request, Dgform $val)
    {
      $editForm = $this->createForm('AppBundle\Form\DgformUpdateType', $val);
      $editForm->handleRequest($request);
      $deleteForm = $this->createDeleteForm($val);

      if ($editForm->isSubmitted() && $editForm->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUsername();
        $val->setUsermodi($user);
        $em->flush();

        $this->addFlash('info', 'Entry Updated!');
      }

      $post = $this->getDoctrine()->getRepository('AppBundle:Dgform')->findAll();

      return $this->render('dgform/edit.html.twig', [
          'appdata2' => $post,
          'appdata' => $val,
          'edit_form' => $editForm->createView(),
          'delete_form' => $deleteForm->createView(),
      ]);
    }

    /**
     * Creates a new entity.
     *
     * @Route("/new", name="dgform_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $val = new Dgform();
        $form = $this->createForm('AppBundle\Form\DgformCreateType', $val);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->container->get('security.token_storage')->getToken()->getUsername();
            $em->persist($val);
            $val->setUsercrea($user);
            $em->flush();

            $this->addFlash('success', 'New Entry Created!');

            return $this->redirectToRoute('dgform_edit', array('formdg' => $val->getFormdg()));
        }

        return $this->render('dgform/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes an entity.
     *
     * @Route("/delete/{formdg}", name="dgform_delete", requirements={"formdg"=".+"})
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Dgform $val)
    {
        $form = $this->createDeleteForm($val);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($val);
            $em->flush();

            $this->addFlash('danger', 'Delete Successful!');
        }

        return $this->redirectToRoute('dgform_index');
    }

    /**
     * Creates a form to delete an entity.
     *
     * @param Dgform $val The Form entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Dgform $val)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('dgform_delete', array('formdg' => $val->getFormdg())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

}
