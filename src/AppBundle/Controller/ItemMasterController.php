<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\ItemMaster;
use AppBundle\Entity\ItemMasterExt;
use AppBundle\Entity\Classes;
use AppBundle\Entity\Color;
use AppBundle\Entity\Decorations;
use AppBundle\Entity\Dgform;
use AppBundle\Entity\Families;
use AppBundle\Entity\GlassType;
use AppBundle\Entity\Levels;
use AppBundle\Entity\PackType;
use AppBundle\Entity\Processes;
use AppBundle\Entity\SleepCode;
use AppBundle\Entity\UpcCode;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\DBAL\DBALException;

/**
 * Decorations table controller.
 *
 * @Route("itemmaster")
 */
class ItemMasterController extends Controller
{
    /**
     * View All - Read Only.
     *
     * @Route("/", name="im_itemmaster")
     * @Method("GET")
     */
    public function homeAction()
    {
        return $this->render('item/home.html.twig');
    }


    /**
     *
     * @Route("/insert", name="im_insert")
     * @Method({"GET", "POST"})
     */
    public function insert(Request $request)
    {
        return $this->render('item/new.html.twig');
    }


    /**
     *
     * @Route("/insertConfirm",name="im_insert_confirm")
     * @Method({"GET", "POST"})
     */
    public function insert_confirm(Request $request)
    {
        $post = Request::createFromGlobals();
        $em = $this->getDoctrine()->getManager();

        $itemmasterObj = new ItemMaster();
        $itemmasterObj = $request->getSession()->get('itemmaster');

        $itemmasterextObj = new ItemMasterExt();
        $itemmasterextObj = $request->getSession()->get('itemmasterext');
        try {
            $em->persist($itemmasterObj);
            $em->persist($itemmasterextObj);
        } catch (DBALException $e) {
            echo $e->getMessage();
            exit;
        }
        $em->flush();

          $this->addFlash('info', 'Item number inserted successfully!');
        return $this->render('item/home.html.twig');
    }




    /**
     *
     * @Route("/details", name="im_details")
     * @Method({"GET", "POST"})
     */
    public function details(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $class = $request->get('class');
        $srlevel = $request->get('srlevel');
        $manprocess = $request->get('manprocess');
        $decoration = $request->get('decoration');
        $form = $request->get('form');
        $glasstype = $request->get('glasstype');
        $colorcode = $request->get('colorcode');
        $upc = $request->get('upc');
        $sleepcode = $request->get('sleepcode');
        $collection = $request->get('collection');
        $itemtype = $request->get('itemtype');
        $packtype = $request->get('packtype');

        $repository = $this->getDoctrine()->getRepository(Classes::class);
        $classObj = $repository->findOneBy(array('class' => $class));

        $repository1 = $this->getDoctrine()->getRepository(Levels::class);
        $levelObj = $repository1->findOneBy(array('srLevel' => $srlevel));

        $repository2 = $this->getDoctrine()->getRepository(Processes::class);
        $processObj = $repository2->findOneBy(array('manProcess' => $manprocess));

        $repository3 = $this->getDoctrine()->getRepository(Decorations::class);
        $decorationObj = $repository3->findOneBy(array('decoration' => $decoration));

        $repository4 = $this->getDoctrine()->getRepository(Dgform::class);
        $formObj = $repository4->findOneBy(array('formdg' => $form));

        $repository5 = $this->getDoctrine()->getRepository(GlassType::class);
        $glasstypeObj = $repository5->findOneBy(array('glasstype' => $glasstype));

        $repository6 = $this->getDoctrine()->getRepository(Color::class);
        $colorObj = $repository6->findOneBy(array('colorcode' => $colorcode));

        $repository7 = $this->getDoctrine()->getRepository(UpcCode::class);
        $upcObj = $repository7->findOneBy(array('upc' => $upc));

        $repository8 = $this->getDoctrine()->getRepository(SleepCode::class);
        $sleepcodeObj = $repository8->findOneBy(array('sleepcode' => $sleepcode));

        $repository11 = $this->getDoctrine()->getRepository(Families::class);
        $familyObj = $repository11->findOneBy(array('family' => $collection));

        $repository12 = $this->getDoctrine()->getRepository(PackType::class);
        $packtypeObj = $repository12->findOneBy(array('packtype' => $packtype));




        if ($classObj==null || $levelObj==null || $processObj==null || $decorationObj==null || $packtypeObj==null || $itemtype!=1 || $itemtype!=4 ||
         $formObj==null || $glasstypeObj==null || $colorObj==null || $upcObj==null || $sleepcodeObj==null || $familyObj==null) {

           $this->addFlash('info', 'Please enter valid itemtype, packtype, class, level, family, process, decoration, form, glasstype, color, sleepcode!');

        } else {
            $repository9 = $this->getDoctrine()->getRepository(ItemMaster::class);
            $itemmasterObj = $repository9->findOneBy(array('itemnumber' => $request->get('itemnumber')));

            $repository10 = $this->getDoctrine()->getRepository(ItemMasterExt::class);
            $itemmasterextObj = $repository10->findOneBy(array('itemnumber' => $request->get('itemnumber')));

            if ($itemmasterObj == null && $itemmasterextObj == null) {
                $itemmasterObjin = new ItemMaster();
                $itemmasterextObjin = new ItemMasterExt();

                $itemmasterObjin->setItemnumber($request->get('itemnumber'));
                $itemmasterextObjin->setItemnumber($request->get('itemnumber'));

                $itemmasterObjin->setDescription($request->get('description'));
                $itemmasterObjin->setDesvariante($request->get('desvariante'));
                $itemmasterObjin->setTempered($request->get('tempered'));
                $itemmasterObjin->setIscommercial($request->get('iscommercial'));
                $itemmasterObjin->setEnamel($request->get('enamel'));
                $itemmasterObjin->setGold($request->get('gold'));
                $itemmasterObjin->setNomenclature($request->get('nomenclature'));
                $itemmasterObjin->setVcanumber($request->get('vcanumber'));
                $itemmasterObjin->setUfile($request->get('ufile'));
                $itemmasterObjin->setGrossweight($request->get('grossweight'));
                $itemmasterObjin->setNetweight($request->get('netweight'));
                $itemmasterObjin->setPackaging($request->get('packaging'));
                $itemmasterObjin->setUnitmeasure($request->get('unitmeasure'));
                $itemmasterObjin->setNbmast($request->get('nbmast'));
                $itemmasterObjin->setLength($request->get('length'));
                $itemmasterObjin->setWidth($request->get('width'));
                $itemmasterObjin->setHeight($request->get('height'));
                $itemmasterObjin->setUnitpallet($request->get('unitpallet'));
                $itemmasterObjin->setCostvca($request->get('costvca'));
                $itemmasterObjin->setMini($request->get('mini'));
                $itemmasterObjin->setMaxi($request->get('maxi'));
                $itemmasterObjin->setCplan($request->get('cplan'));
                $itemmasterObjin->setLaser($request->get('laser'));
                $itemmasterObjin->setItemtype($request->get('itemtype'));

                $itemmasterObjin->setPacktype($packtypeObj->getPackType());

                $itemmasterObjin->setCollection($familyObj->getFamily());

                $itemmasterObjin->setClass($classObj->getClass());

                $itemmasterObjin->setSrLevel($levelObj->getSrLevel());

                $itemmasterObjin->setManProcess($processObj->getManProcess());

                $itemmasterObjin->setDecoration($decorationObj->getDecoration());

                $itemmasterObjin->setForm($formObj->getFormdg());

                $itemmasterObjin->setCtver($glasstypeObj->getGlasstype());

                $itemmasterObjin->setCteinte($colorObj->getColorcode());

                $itemmasterObjin->setUpc($upcObj->getUpc());

                $itemmasterObjin->setSleepcode($sleepcodeObj->getSleepcode());

                $itemmasterextObjin->setCorpsku($request->get('corpsku'));
                $itemmasterextObjin->setExtmerchcat($request->get('extmerchcat'));
                $itemmasterextObjin->setUpc($request->get('upccode'));
                $itemmasterextObjin->setCasecode($request->get('casecode'));



                $request->getSession()->set('itemmaster', $itemmasterObjin);
                $request->getSession()->set('itemmasterext', $itemmasterextObjin);


                return $this->render('item/insert.html.twig', [
                'itemmasterObj'=>$itemmasterObjin,
                'itemmasterextObj'=>$itemmasterextObjin,
                'classObj'=>$classObj,
                'familyObj'=>$familyObj,
                'levelObj'=>$levelObj,
                'processObj'=>$processObj,
                'decorationObj'=>$decorationObj,
                'formObj'=>$formObj,
                'glasstypeObj'=>$glasstypeObj,
                'colorObj'=>$colorObj,
                'upcObj'=>$upcObj,
                'sleepcodeObj'=>$sleepcodeObj
              ]);
            } else {
                $this->addFlash('info', 'Itemnumber already exists!');
            }
        }
        return $this->render('item/new.html.twig');
    }





    /**
     *
     * @Route("/display", name="itemnumber_display")
     * @Method({"GET", "POST"})
     */
    public function display(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->query->get('myInput1');


        $query = $em
       ->createQuery(
       "SELECT i,l,ie,c,co,d,f,fa,g,p,pr,sc,u
       FROM AppBundle:ItemMaster i
       left join AppBundle:Levels l with i.srlevel = l.srLevel
       left join AppBundle:ItemMasterExt ie with i.itemnumber=ie.itemnumber
       left join AppBundle:Classes c with i.class=c.class
       left join AppBundle:Color co with i.cteinte = co.colorcode
       left join AppBundle:Decorations d with i.decoration = d.decoration
       left join AppBundle:Dgform f with i.form=f.formdg
       left join AppBundle:Families fa with i.collection = fa.family
       left join AppBundle:GlassType g with i.ctver = g.glasstype
       left join AppBundle:PackType p with i.packtype = p.packtype
       left join AppBundle:Processes pr with i.manprocess = pr.manProcess
       left join AppBundle:SleepCode sc with i.sleepcode = sc.sleepcode
       left join AppBundle:UpcCode u with i.upc = u.upc
       WHERE i.itemnumber = '".$data."'"
     );


        $post = $query->getResult();
        dump($query);


        if ($post == null) {
            $this->addFlash('info', 'Item number not found!');
            return $this->render('item/home.html.twig');
        }


        return $this->render('item/index.html.twig', [
        'itemmasterData' => $post[0],
        'levelsData' => $post[1],
        'itemmasterextData'=> $post[2],
        'classesData'=> $post[3],
        'colorData'=> $post[4],
        'decorationsData'=> $post[5],
        'formData'=> $post[6],
        'familiesData'=> $post[7],
        'glasstypeData'=> $post[8],
        'packtypeData'=> $post[9],
        'processesData'=> $post[10],
        'sleepcodeData'=> $post[11],
        'upccodeData'=> $post[12],
      ]);
    }


    /**
     *
     * @Route("/edit/{itemnumberEdit}", name="itemmaster_edit", requirements={"itemnumberEdit"=".+"})
     * @Method({"GET", "POST"})
     */
    public function edit(Request $request)
    {
        $val = $request->attributes->get('itemnumberEdit');

        $em = $this->getDoctrine()->getManager();
        $data = $val;
        $query = $em
       ->createQuery(
         "SELECT i,l,ie,c,co,d,f,fa,g,p,pr,sc,u
         FROM AppBundle:ItemMaster i
         left join AppBundle:Levels l with i.srlevel = l.srLevel
         left join AppBundle:ItemMasterExt ie with i.itemnumber=ie.itemnumber
         left join AppBundle:Classes c with i.class=c.class
         left join AppBundle:Color co with i.cteinte = co.colorcode
         left join AppBundle:Decorations d with i.decoration = d.decoration
         left join AppBundle:Dgform f with i.form=f.formdg
         left join AppBundle:Families fa with i.collection = fa.family
         left join AppBundle:GlassType g with i.ctver = g.glasstype
         left join AppBundle:PackType p with i.packtype = p.packtype
         left join AppBundle:Processes pr with i.manprocess = pr.manProcess
         left join AppBundle:SleepCode sc with i.sleepcode = sc.sleepcode
         left join AppBundle:UpcCode u with i.upc = u.upc
         WHERE i.itemnumber = '".$data."'"
       );

        $post = $query->getResult();



        return $this->render('item/edit.html.twig', [
        'itemmasterData' => $post[0],
        'levelsData' => $post[1],
        'itemmasterextData'=> $post[2],
        'classesData'=> $post[3],
        'colorData'=> $post[4],
        'decorationsData'=> $post[5],
        'formData'=> $post[6],
        'familiesData'=> $post[7],
        'glasstypeData'=> $post[8],
        'packtypeData'=> $post[9],
        'processesData'=> $post[10],
        'sleepcodeData'=> $post[11],
        'upccodeData'=> $post[12],
      ]);
    }

    /**
     *
     * @Route("/update", name="itemmaster_update")
     * @Method({"GET", "POST"})
     */
    public function update(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->query->all();

        $itemnumber = $data['itemnumber'];
        $description = $data['description'];
        $desvariante = $data['desvariante'];
        $tempered = $data['tempered'];
        $iscommercial = $data['iscommercial'];
        $enamel = $data['enamel'];
        $gold = $data['gold'];
        $nomenclature = $data['nomenclature'];
        $vcanumber = $data['vcanumber'];
        $ufile = $data['ufile'];
        $grossweight = $data['grossweight'];
        $netweight = $data['netweight'];
        $packaging = $data['packaging'];
        $unitmeasure = $data['unitmeasure'];
        $nbmast = $data['nbmast'];
        $length = $data['length'];
        $width = $data['width'];
        $height = $data['height'];
        $unitpallet = $data['unitpallet'];
        $costvca = $data['costvca'];
        $mini = $data['mini'];
        $maxi = $data['maxi'];
        $sleepdate = $data['sleepdate'];
        $creationdate = $data['creationdate'];
        $moddate = $data['moddate'];
        $cplan = $data['cplan'];
        $laser = $data['laser'];

        $collection = $data['collection'];

        $class = $data['class'];
        if($class != null){
          $desclass = $data['desclass'];
        }

        $srlevel = $data['srlevel'];
        if($srlevel != null){
          $levelsdescription = $data['levelsdescription'];
        }

        $manprocess = $data['manprocess'];
        if($manprocess != null){

          $manprocessdescription = $data['manprocessdescription'];
        }
        $manprocessdescription = $data['manprocessdescription'];

        $decoration = $data['decoration'];
        if($decoration != null){
            $screens = $data['screens'];
        }


        $form = $data['form'];
        if($form != null){
            $formdescription = $data['formdescription'];
        }


        $glasstype = $data['glasstype'];
        if($glasstype != null){
            $englishtype = $data['englishtype'];
        }


        $colorcode = $data['colorcode'];
        if($colorcode != null){
          $colorenglish = $data['colorenglish'];
        }
        if($data['corpsku'] != null){
          $corpsku = $data['corpsku'];
        }
        if($data['extmerchcat'] != null){
          $extmerchcat = $data['extmerchcat'];
        }
        if($data['upccode'] != null){
          $upccode = $data['upccode'];
        }
        if($data['casecode'] != null){
          $casecode = $data['casecode'];
        }

        $upc = $data['upc'];
        if($upc != null){
          $upcdescription = $data['upcdescription'];
        }

        $sleepcode = $data['sleepcode'];
        if($sleepcode != null){
          $sleepcodedescription = $data['sleepcodedescription'];
        }

        $em = $this->getDoctrine()->getManager();

        $itemmasterT = $em->find('AppBundle:ItemMaster', $itemnumber);
        $itemmasterextT = $em->find('AppBundle:ItemMasterExt', $itemnumber);
        $classT = $em->getRepository('AppBundle:Classes')->findOneBy(array('class' => $class));
        $colorT = $em->getRepository('AppBundle:Color')->findOneBy(array('colorcode' => $colorcode));
        $decorationT = $em->getRepository('AppBundle:Decorations')->findOneBy(array('decoration' => $decoration));
        $formT = $em->getRepository('AppBundle:Dgform')->findOneBy(array('formdg' => $form));
        $collectionT = $em->getRepository('AppBundle:Families')->findOneBy(array('family' => $collection));
        $glasstypeT = $em->getRepository('AppBundle:GlassType')->findOneBy(array('glasstype' => $glasstype));
        $srlevelT = $em->getRepository('AppBundle:Levels')->findOneBy(array('srLevel' => $srlevel));
        $manprocessT = $em->getRepository('AppBundle:Processes')->findOneBy(array('manProcess' => $manprocess));
        $sleepcodeT = $em->getRepository('AppBundle:SleepCode')->findOneBy(array('sleepcode' => $sleepcode));
        $upcT = $em->getRepository('AppBundle:UpcCode')->findOneBy(array('upc' => $upc));

        $itemmasterT->setClass($class);
        if($classT != null){
          $classT->setClass($class);
          $classT->setDesclass($desclass);
        }

        $itemmasterT->setSrlevel((int)$srlevel);
        if($srlevelT != null){

          $srlevelT->setSrLevel((int)$srlevel);
          $srlevelT->setDescription($levelsdescription);
        }

        $itemmasterT->setManprocess($manprocess);
        if($manprocessT != null){

          $manprocessT->setManProcess($manprocess);
          $manprocessT->setDescription($manprocessdescription);
        }

        $itemmasterT->setDecoration($decoration);

        if($decorationT != null){
          $decorationT->setDecoration($decoration);
          $decorationT->setScreens($screens);
        }

        $itemmasterT->setForm($form);
        if($formT != null){
          $formT->setFormdg($form);
          $formT->setDescription($formdescription);
        }


        $itemmasterT->setCtver($glasstype);
        if($glasstypeT != null){

          $glasstypeT->setGlasstype($glasstype);
          $glasstypeT->setEnglishtype($englishtype);
        }

        $itemmasterT->setCteinte($colorcode);
        if($colorT != null){

          $colorT->setColorcode($colorcode);
          $colorT->setColorenglish($colorenglish);
        }

        $itemmasterT->setUpc((int)$upc);
        if($upcT != null){

          $upcT->setUpc((int)$upc);
          $upcT->setDescription($upcdescription);
        }

        $itemmasterT->setSleepcode((int)$sleepcode);
        if($sleepcodeT != null){
          $sleepcodeT->setSleepcode((int)$sleepcode);
          $sleepcodeT->setDescription($sleepcodedescription);
        }

        $itemmasterT->setCollection($collection);
        if($collectionT != null){
          $collectionT->setFamily($collection);
        }

        if($itemmasterextT != null){
          $itemmasterextT->setItemnumber($itemnumber);
          $itemmasterextT->setCorpsku($corpsku);
          $itemmasterextT->setUpc($upccode);
          $itemmasterextT->setCasecode($casecode);
          $itemmasterextT->setExtmerchcat($extmerchcat);
        }


        $itemmasterT->setItemnumber($itemnumber);
        $itemmasterT->setDescription($description);
        $itemmasterT->setDesvariante($desvariante);
        $itemmasterT->setTempered($tempered);
        $itemmasterT->setIscommercial((int)$iscommercial);
        $itemmasterT->setEnamel(floatval($enamel));
        $itemmasterT->setGold(floatval($gold));
        $itemmasterT->setNomenclature($nomenclature);
        $itemmasterT->setVcanumber($vcanumber);
        $itemmasterT->setUfile($ufile);
        $itemmasterT->setGrossweight(floatval($grossweight));
        $itemmasterT->setNetweight(floatval($netweight));
        $itemmasterT->setPackaging($packaging);
        $itemmasterT->setUnitmeasure((int)$unitmeasure);
        $itemmasterT->setNbmast((int)$nbmast);
        $itemmasterT->setLength(floatval($length));
        $itemmasterT->setWidth(floatval($width));
        $itemmasterT->setHeight(floatval($height));
        $itemmasterT->setUnitpallet((int)$unitpallet);
        $itemmasterT->setCostvca(floatval($costvca));
        $itemmasterT->setMini(floatval($mini));
        $itemmasterT->setMaxi(floatval($maxi));
        //$itemmasterT->setSleepdate($sleepdate);
        //$itemmasterT->setCreationdate($creationdate);
        //$itemmasterT->setModdate($moddate);
        $itemmasterT->setCplan($cplan);
        $itemmasterT->setLaser($laser);


        $em->persist($itemmasterT);

        if($itemmasterextT!=null){
          $em->persist($itemmasterextT);
        }
        if($sleepcodeT!=null){
          $em->persist($sleepcodeT);
        }
        if($manprocessT!=null){
          $em->persist($manprocessT);
        }
        if($srlevelT!=null){
          $em->persist($srlevelT);
        }
        if($glasstypeT!=null){
          $em->persist($glasstypeT);
        }
        if($collectionT!=null){
          $em->persist($collectionT);
        }
        if($decorationT!=null){
          $em->persist($decorationT);
        }
        if($formT!=null){
          $em->persist($formT);
        }
        if($colorT!=null){
          $em->persist($colorT);
        }
        if($classT!=null){
          $em->persist($classT);
        }
        if($upcT!=null){
          $em->persist($upcT);
        }

        $em->flush();

        $this->addFlash('info', 'Successfully updated');

        return $this->render('item/home.html.twig');
    }
}
