<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\GlassType;

/**
 * glasstypes table controller.
 *
 * @Route("GlassType")
 */

 class GlassTypeController extends Controller
 {
     /**
      * View All.
      *
      * @Route("/index", name="glasstypes_index")
      * @Method("GET")
      */


     public function viewAction()
     {
         $em = $this->getDoctrine()->getManager();

         $post = $this->getDoctrine()->getRepository('AppBundle:GlassType')->findAll();

         return $this->render('glasstypes/index.html.twig', [
        'appdata' => $post
      ]);
     }


     /**
      * Select Entity Value Page.
      *
      * @Route("/view_all", name="glasstypes_select")
      * @Method("GET")
      */

     public function selectAction(Request $request)
     {
         $em = $this->getDoctrine()->getManager();

         if ($request->query->get('selectbox1')) {
             return $this->redirectToRoute('glasstypes_edit', array('glasstype' => $request->query->get('selectbox1')));
         }

         $post = $this->getDoctrine()->getRepository('AppBundle:GlassType')->findAll();

         return $this->render('glasstypes/select.html.twig', [
        'appdata' => $post
      ]);
     }

     /**
            * Finds and displays entity value to edit.
            *
            * @Route("/edit/{glasstype}", name="glasstypes_edit", requirements={"glasstype"=".+"})
            * @Method({"GET", "POST"})
            */

     public function showAction(Request $request, GlassType $val)
     {
         $editForm = $this->createForm('AppBundle\Form\GlassTypeUpdateType', $val);
         $editForm->handleRequest($request);
         $deleteForm = $this->createDeleteForm($val);

         if ($editForm->isSubmitted() && $editForm->isValid()) {
             $em = $this->getDoctrine()->getManager();
             $em->persist($editForm->getData());
             $em->flush();
             $this->addFlash('info', 'Entry Updated!');
         }


         $post = $this->getDoctrine()->getRepository('AppBundle:GlassType')->findAll();

         return $this->render('glasstypes/edit.html.twig', [
              'appdata2' => $post,
              'appdata' => $val,
              'edit_form' => $editForm->createView(),
               'delete_form' => $deleteForm->createView(),
          ]);
     }


     /**
      * Creates a new entity.
      *l=
      * @Route("/new", name="glasstypes_new")
      * @Method({"GET", "POST"})
      */

     public function newAction(Request $request)
     {
         $val = new GlassType();
         $form = $this->createForm('AppBundle\Form\GlassTypeCreateType', $val);
         $form->handleRequest($request);

         if ($form->isSubmitted() && $form->isValid()) {
             $em = $this->getDoctrine()->getManager();
             $user = $this->container->get('security.token_storage')->getToken()->getUsername();
             $em->persist($val);

             $em->flush();

             $this->addFlash('success', 'New Entry Created!');

             return $this->redirectToRoute('glasstypes_edit', array('glasstype' => $val->getGlasstype()));
         }

         return $this->render('glasstypes/new.html.twig', [
                'form' => $form->createView(),
            ]);
     }

     /**
        * Deletes an entity.
        *
        * @Route("/delete/{glasstype}", name="glasstypes_delete", requirements={"glasstype"=".+"})
        * @Method("DELETE")
        */

     public function deleteAction(Request $request, GlassType $val)
     {
         $form = $this->createDeleteForm($val);
         $form->handleRequest($request);

         if ($form->isSubmitted() && $form->isValid()) {
             $em = $this->getDoctrine()->getManager();
             $em->remove($val);
             $em->flush();

             $this->addFlash('danger', 'Delete Successful!');
         }

         return $this->redirectToRoute('glasstypes_index');
     }

     /**
      * Creates a form to delete an entity.
      *
      * @param GlassType $val The Form entity
      *
      * @return \Symfony\Component\Form\Form The form
      */

     private function createDeleteForm(GlassType $val)
     {
         return $this->createFormBuilder()
                      ->setAction($this->generateUrl('glasstypes_delete', array('glasstype' => $val->getGlasstype())))
                      ->setMethod('DELETE')
                      ->getForm()

                      ;
     }
 }
