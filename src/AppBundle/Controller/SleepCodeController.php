<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\PackType;
use AppBundle\Entity\SleepCode;

/**
 * Levels table controller.
 *
 * @Route("SleepCode")
 */
class SleepCodeController extends Controller
{
    /**
     * View All.
     *
     * @Route("/index", name="sleepcode_index")
     * @Method("GET")
     */
    public function viewAction()
    {
      $em = $this->getDoctrine()->getManager();

      $post = $this->getDoctrine()->getRepository('AppBundle:SleepCode')->findAll();

      return $this->render('sleepcode/index.html.twig',[
        'appdata' => $post
      ]);
    }


    /**
     * Creates a new entity.
     *
     * @Route("/new", name="sleepcode_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $val = new SleepCode();
        $form = $this->createForm('AppBundle\Form\SleepcodeCreateType', $val);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->container->get('security.token_storage')->getToken()->getUsername();
            $em->persist($val);
            $val->setUsercrea($user);
            $val->setUsermodi($user);
            $em->flush();

            $this->addFlash('success', 'New Entry Created!');

            return $this->redirectToRoute('sleepcode_edit', array('sleepcode' => $val->getSleepcode()));
        }

        return $this->render('sleepcode/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }


        /**
         * Select Entity Value Page.
         *
         * @Route("/view_all", name="sleepcode_select")
         * @Method("GET")
         */
        public function selectAction(Request $request)
        {

          $em = $this->getDoctrine()->getManager();
          if ($request->query->get('selectbox1')!="") {

            return $this->redirectToRoute('sleepcode_edit', array('sleepcode' => $request->query->get('selectbox1')));
          }

          $post = $this->getDoctrine()->getRepository('AppBundle:SleepCode')->findAll();
          // dump($post); die;

          return $this->render('sleepcode/select.html.twig',[
            'appdata' => $post
          ]);
        }


            /**
             * Finds and displays entity value to edit.
             *
             * @Route("/edit/{sleepcode}", name="sleepcode_edit", requirements={"sleepcode"=".+"})
             * @Method({"GET", "POST"})
             */
            public function showAction(Request $request, Sleepcode $val)
            {
              $editForm = $this->createForm('AppBundle\Form\SleepcodeUpdateType', $val);
              $editForm->handleRequest($request);
              $deleteForm = $this->createDeleteForm($val);

              if ($editForm->isSubmitted() && $editForm->isValid()) {


                $em = $this->getDoctrine()->getManager();

                $editForm->getData()->updateModifiedDatetime();

                $em->persist($editForm->getData());
                $em->flush();

                $this->addFlash('info', 'Entry Updated!');
              }

              $post = $this->getDoctrine()->getRepository('AppBundle:SleepCode')->findAll();

              return $this->render('sleepcode/edit.html.twig', [
                  'appdata2' => $post,
                  'appdata' => $val,
                  'edit_form' => $editForm->createView(),
                  'delete_form' => $deleteForm->createView()
              ]);
            }



            /**
             * Deletes an entity.
             *
             * @Route("/delete/{sleepcode}", name="sleepcode_delete", requirements={"sleepcode"=".+"})
             * @Method("DELETE")
             */
            public function deleteAction(Request $request, Sleepcode $val)
            {
                $form = $this->createDeleteForm($val);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($val);
                    $em->flush();

                    $this->addFlash('danger', 'Delete Successful!');
                }

                return $this->redirectToRoute('sleepcode_index');
            }

            /**
             * Creates a form to delete an entity.
             *
             * @param SleepCode $val The Form entity
             *
             * @return \Symfony\Component\Form\Form The form
             */
            private function createDeleteForm(Sleepcode $val)
            {
                return $this->createFormBuilder()
                    ->setAction($this->generateUrl('sleepcode_delete', array('sleepcode' => $val->getSleepcode())))
                    ->setMethod('DELETE')
                    ->getForm()
                ;
            }



          }
