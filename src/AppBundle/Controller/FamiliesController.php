<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Families;

/**
 * Families table controller.
 *
 * @Route("families")
 */
class FamiliesController extends Controller
{
    /**
     * View All.
     *
     * @Route("/index", name="families_index")
     * @Method("GET")
     */
    public function viewAction()
    {
      $post = $this->getDoctrine()->getRepository('AppBundle:Families')->findAll();

      return $this->render('families/index.html.twig',[
        'appdata' => $post
      ]);
    }


    /**
     * Select Entity Value Page.
     *
     * @Route("/view_all", name="families_select")
     * @Method("GET")
     */
    public function selectAction(Request $request)
    {

      if ($request->query->get('selectbox1')) {
        return $this->redirectToRoute('families_edit', array('family' => $request->query->get('selectbox1')));
      }

      $post = $this->getDoctrine()->getRepository('AppBundle:Families')->findAll();

      return $this->render('families/select.html.twig',[
        'appdata' => $post
      ]);
    }


        /**
         * Finds and displays entity value to edit.
         *
         * @Route("/edit/{family}", name="families_edit", requirements={"family"=".+"})
         * @Method({"GET", "POST"})
         */
        public function showAction(Request $request, Families $val)
        {
          $editForm = $this->createForm('AppBundle\Form\FamiliesUpdateType', $val);
          $editForm->handleRequest($request);
          $deleteForm = $this->createDeleteForm($val);

          if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->container->get('security.token_storage')->getToken()->getUsername();
            $val->setUsermodi($user);
            $em->flush();

            $this->addFlash('info', 'Entry Updated!');
          }

          $post = $this->getDoctrine()->getRepository('AppBundle:Families')->findAll();

          return $this->render('families/edit.html.twig', [
              'appdata2' => $post,
              'appdata' => $val,
              'edit_form' => $editForm->createView(),
              'delete_form' => $deleteForm->createView(),
          ]);
        }



            /**
             * Creates a new entity.
             *
             * @Route("/new", name="families_new")
             * @Method({"GET", "POST"})
             */
            public function newAction(Request $request)
            {
                $val = new Families();
                $form = $this->createForm('AppBundle\Form\FamiliesUpdateType', $val);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    $user = $this->container->get('security.token_storage')->getToken()->getUsername();
                    $em->persist($val);
                    $val->setUsercrea($user);
                    $em->flush();

                    $this->addFlash('success', 'New Entry Created!');

                    return $this->redirectToRoute('families_edit', array('family' => $val->getFamily()));
                }

                return $this->render('families/new.html.twig', [
                    'form' => $form->createView(),
                ]);
            }


            /**
             * Deletes an entity.
             *
             * @Route("/delete/{family}", name="families_delete", requirements={"family"=".+"})
             * @Method("DELETE")
             */
            public function deleteAction(Request $request, Families $val)
            {
                $form = $this->createDeleteForm($val);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($val);
                    $em->flush();

                    $this->addFlash('danger', 'Delete Successful!');
                }

                return $this->redirectToRoute('families_index');
            }

            /**
             * Creates a form to delete an entity.
             *
             * @param Families $val The Form entity
             *
             * @return \Symfony\Component\Form\Form The form
             */
            private function createDeleteForm(Families $val)
            {
                return $this->createFormBuilder()
                    ->setAction($this->generateUrl('families_delete', array('family' => $val->getFamily())))
                    ->setMethod('DELETE')
                    ->getForm()
                ;
            }


}
