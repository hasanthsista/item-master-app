<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\PackType;

/**
 * Levels table controller.
 *
 * @Route("PackType")
 */
class PackTypeController extends Controller
{
    /**
     * View All.
     *
     * @Route("/index", name="packtype_index")
     * @Method("GET")
     */
    public function viewAction()
    {
      $em = $this->getDoctrine()->getManager();

      $post = $this->getDoctrine()->getRepository('AppBundle:PackType')->findAll();

      return $this->render('packtype/index.html.twig',[
        'appdata' => $post
      ]);
    }


    /**
     * Creates a new entity.
     *
     * @Route("/new", name="packtype_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $val = new PackType();
        $form = $this->createForm('AppBundle\Form\PacktypeCreateType', $val);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->container->get('security.token_storage')->getToken()->getUsername();
            $em->persist($val);
            $val->setUsercrea($user);
            $val->setUsermodi($user);
            $em->flush();

            $this->addFlash('success', 'New Entry Created!');

            return $this->redirectToRoute('packtype_edit', array('packtype' => $val->getPackType()));
        }

        return $this->render('packtype/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }


        /**
         * Select Entity Value Page.
         *
         * @Route("/view_all", name="packtype_select")
         * @Method("GET")
         */
        public function selectAction(Request $request)
        {

          $em = $this->getDoctrine()->getManager();
          if ($request->query->get('selectbox1')!="") {

            return $this->redirectToRoute('packtype_edit', array('packtype' => $request->query->get('selectbox1')));
          }

          $post = $this->getDoctrine()->getRepository('AppBundle:PackType')->findAll();
          // dump($post); die;

          return $this->render('packtype/select.html.twig',[
            'appdata' => $post
          ]);
        }


            /**
             * Finds and displays entity value to edit.
             *
             * @Route("/edit/{packtype}", name="packtype_edit", requirements={"packtype"=".+"})
             * @Method({"GET", "POST"})
             */
            public function showAction(Request $request, Packtype $val)
            {
              $editForm = $this->createForm('AppBundle\Form\PacktypeUpdateType', $val);
              $editForm->handleRequest($request);
              $deleteForm = $this->createDeleteForm($val);

              if ($editForm->isSubmitted() && $editForm->isValid()) {

                // $newEntry = new PackType();
                // $newEntry->setDescription($request->request->get('inputName'));
                // $em->persist($newEntry);
                // $em->flush();
                //$user = $this->container->get('security.token_storage')->getToken()->getUsername();
                //$val->setUsermodi($user);

                $em = $this->getDoctrine()->getManager();

                $editForm->getData()->updateModifiedDatetime();

                $em->persist($editForm->getData());
                $em->flush();

                $this->addFlash('info', 'Entry Updated!');
              }

              $post = $this->getDoctrine()->getRepository('AppBundle:PackType')->findAll();

              return $this->render('packtype/edit.html.twig', [
                  'appdata2' => $post,
                  'appdata' => $val,
                  'edit_form' => $editForm->createView(),
                  'delete_form' => $deleteForm->createView()
              ]);
            }



            /**
             * Deletes an entity.
             *
             * @Route("/delete/{packtype}", name="packtype_delete", requirements={"packtype"=".+"})
             * @Method("DELETE")
             */
            public function deleteAction(Request $request, PackType $val)
            {
                $form = $this->createDeleteForm($val);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($val);
                    $em->flush();

                    $this->addFlash('danger', 'Delete Successful!');
                }

                return $this->redirectToRoute('packtype_index');
            }

            /**
             * Creates a form to delete an entity.
             *
             * @param PackType $val The Form entity
             *
             * @return \Symfony\Component\Form\Form The form
             */
            private function createDeleteForm(PackType $val)
            {
                return $this->createFormBuilder()
                    ->setAction($this->generateUrl('packtype_delete', array('packtype' => $val->getpacktype())))
                    ->setMethod('DELETE')
                    ->getForm()
                ;
            }



          }
