<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\Levels;

class PacktypeUpdateType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
      $builder
        ->add('packtype', TextType::class,array('label'=>'Pack Type','disabled'=>true))
         ->add('description', TextType::class,array('label'=>'Description'))
         ->add('usercrea', TextType::class,array('label'=>'Created by user','disabled'=>true))
         ->add('datecrea', DateTimeType::class,array('label'=>'Created on','disabled'=>true))
         ->add('usermodi', TextType::class,array('label'=>'Modified by user'))
         ->add('datemodi', DateTimeType::class,array('label'=>'Modified on'))
      ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
      $resolver->setDefaults([
          'data_class' => 'AppBundle\Entity\PackType'
      ]);
  }

}
