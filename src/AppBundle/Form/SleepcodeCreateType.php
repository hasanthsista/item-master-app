<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\SleepCode;

class SleepcodeCreateType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
      $builder
      ->add('sleepcode', IntegerType::class,array('label'=>'Sleepcode'))
       ->add('description', TextType::class,array('label'=>'Description'))
       ->add('datecrea', DateTimeType::class,array('label'=>'Created on','disabled'=>true))
       ->add('datemodi', DateTimeType::class,array('label'=>'Modified on','disabled'=>true))
      
        // ->add('datecrea', DateTimeType::class)
        // ->add('usermodi', TextType::class)
        // ->add('datemodi', DateTimeType::class)
      ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
      $resolver->setDefaults([
          'data_class' => 'AppBundle\Entity\SleepCode'
      ]);
  }

}
