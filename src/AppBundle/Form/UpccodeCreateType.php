<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\UpcCode;

class UpccodeCreateType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
      $builder
      ->add('upc', IntegerType::class,array('label'=>'Upc'))
       ->add('description', TextType::class,array('label'=>'Description'))
       ->add('datecrea', DateTimeType::class,array('label'=>'Created on','disabled'=>true))
      ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
      $resolver->setDefaults([
          'data_class' => 'AppBundle\Entity\UpcCode'
      ]);
  }

}
