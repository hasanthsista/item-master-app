<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\ItemMaster;

class ItemMasterCreateType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {

      $builder
       ->add('itemnumber', TextType::class,array('label' => 'Item Number'))
       ->add('description', TextType::class,array('label' => 'Description'))
       ->add('desvariante', TextType::class,array('label' => 'Var'))
       ->add('class', ChoiceType::class,array('label' => 'Class'))
       ->add('decoration', TextType::class,array('label' => 'Decoration'))
       ->add('upc', TextType::class)
       ->add('itemtype', TextType::class)
       ->add('packtype', TextType::class)
       ->add('collection', TextType::class)
       ->add('nomenclature', TextType::class)
       ->add('manprocess', TextType::class)
       ->add('srlevel', TextType::class,array('label' => 'Level '))
       ->add('tempered', TextType::class)
       ->add('netweight', TextType::class)
       ->add('grossweight', TextType::class)
       ->add('lggrosswt', TextType::class)
       ->add('enamel', TextType::class)
       ->add('gold', TextType::class)
       ->add('packaging', TextType::class)
       ->add('unitmeasure', TextType::class)
       ->add('unitpallet', TextType::class)
       ->add('totalcost', TextType::class)
       ->add('seccost', TextType::class)
       ->add('sleepcode', TextType::class)
       ->add('nbmast', TextType::class)
       ->add('mini', TextType::class)
       ->add('maxi', TextType::class)
       ->add('mosale', TextType::class)
       ->add('lifeft', TextType::class)
       ->add('supplier', TextType::class)
       ->add('nb', TextType::class)
       ->add('width', TextType::class)
       ->add('height', TextType::class)
       ->add('length', TextType::class)
       ->add('cells', TextType::class)
       ->add('nb2', TextType::class)
       ->add('height2', TextType::class)
       ->add('length2', TextType::class)
       ->add('cells2', TextType::class)
       ->add('packqual', TextType::class)
       ->add('vcanumber', TextType::class)
       ->add('moldtype', TextType::class)
       ->add('nbbulk', TextType::class)
       ->add('ufile', TextType::class)
       ->add('ctver', TextType::class,array('label' => 'Glass Code'))
       ->add('cteinte', TextType::class,array('label' => 'Color'))
       ->add('cplan', TextType::class)
       ->add('costpk', TextType::class)
       ->add('costvca', TextType::class,array('label' => 'VCA Cost'))
       ->add('iscommercial', TextType::class,array('label' => 'Is Commercial ?'))
       ->add('laser', TextType::class)
       ->add('creationdate', DateTimeType::class,['disabled'=>true],array('label' => 'Created Date'))
       ->add('moddate', DateTimeType::class,['disabled'=>true],array('label' => 'Modified Date'))
       ->add('sleepdate', DateTimeType::class,array('label' => 'Sleep Date'))
      ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
      $resolver->setDefaults([
          'data_class' => 'AppBundle\Entity\ItemMaster'
      ]);
  }

}
