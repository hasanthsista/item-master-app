<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\Processes;

class ProcessesCreateType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
      $builder
      ->add('manProcess', TextType::class,array('label'=>'Process'))
       ->add('description', TextType::class,array('label'=>'Description'))
       ->add('mini', TextType::class,array('label'=>'Mini'))
       ->add('maxi', TextType::class,array('label'=>'Maxi'))
       ->add('grlevel', TextType::class,array('label'=>'GR Level'))
       ->add('csecact', TextType::class,array('label'=>'Csecact'))
       //->add('usercrea', TextType::class)
       // ->add('datecrea', DateTimeType::class,['disabled'=>true])
       // ->add('datemodi', DateTimeType::class,['disabled'=>true])
        // ->add('usercrea', TextType::class)
        // ->add('datecrea', DateTimeType::class)
        // ->add('usermodi', TextType::class)
        // ->add('datemodi', DateTimeType::class)
      ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
      $resolver->setDefaults([
          'data_class' => 'AppBundle\Entity\Processes'
      ]);
  }

}
