<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\UpcCode;

class UpccodeUpdateType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
      $builder
      ->add('upc', IntegerType::class,array('label'=>'Upc'))
       ->add('description', TextType::class,array('label'=>'Description'))
       ->add('usercrea', TextType::class,array('label'=>'Created by user','disabled'=>true))
       ->add('datecrea', DateTimeType::class,array('disabled'=>true,'label'=>'Created on'))
       ->add('datemodi', DateTimeType::class,array('disabled'=>true,'label'=>'Modified on'))
        ->add('usermodi', TextType::class,array('label'=>'Modified by user'))
        // ->add('datecrea', DateTimeType::class)
        // ->add('usermodi', TextType::class)
        // ->add('datemodi', DateTimeType::class)
      ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
      $resolver->setDefaults([
          'data_class' => 'AppBundle\Entity\UpcCode'
      ]);
  }

}
