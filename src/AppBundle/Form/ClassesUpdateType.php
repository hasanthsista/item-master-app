<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\Classes;




class ClassesUpdateType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
       ->add('class', TextType::class,array('label'=>'Class'))
        ->add('desclass', TextType::class,array('label'=>'Description'))
        ->add('line', TextType::class,array('label'=>'Line'))
     ;
 }

 public function configureOptions(OptionsResolver $resolver)
 {
     $resolver->setDefaults([
         'data_class' => 'AppBundle\Entity\Classes'
     ]);
 }

}

 ?>
