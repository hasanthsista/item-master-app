<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\Levels;

class LevelsUpdateType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
      $builder
        ->add('srLevel', TextType::class, [
          'disabled' => true,
        ],array('label'=>'Level'))
         ->add('runmini', TextType::class)
         ->add('changewait', TextType::class,array('label'=>'Change Wait'))
         ->add('grlevel', TextType::class,array('label'=>'GR Level'))
         ->add('grdescrip', TextType::class,array('label'=>'GR Description'))
        // ->add('usercrea', TextType::class)
        // ->add('datecrea', DateTimeType::class)
        // ->add('usermodi', TextType::class)
        // ->add('datemodi', DateTimeType::class)
      ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
      $resolver->setDefaults([
          'data_class' => 'AppBundle\Entity\Levels'
      ]);
  }

}
