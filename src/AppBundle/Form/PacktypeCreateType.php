<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\PackType;

class PacktypeCreateType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
      $builder
      ->add('packtype', TextType::class,array('label'=>'Pack Type'))
       ->add('description', TextType::class,array('label'=>'Description'))
       //->add('usercrea', TextType::class)
       // ->add('datecrea', DateTimeType::class,['disabled'=>true])
       // ->add('datemodi', DateTimeType::class,['disabled'=>true])
        // ->add('usercrea', TextType::class)
        // ->add('datecrea', DateTimeType::class)
        // ->add('usermodi', TextType::class)
        // ->add('datemodi', DateTimeType::class)
      ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
      $resolver->setDefaults([
          'data_class' => 'AppBundle\Entity\PackType'
      ]);
  }

}
