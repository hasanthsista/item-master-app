<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\GlassType;


class GlassTypeUpdateType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
    ->add('glasstype', TextType::class,array('label'=>'Glass Type'))
    ->add('englishtype', TextType::class,array('label'=>'Description'))
    ->add('heattrtmtcode', TextType::class,array('label'=>'Heat_Trtmt'))
    ->add('coldtrtmtcode', TextType::class,array('label'=>'Cold_Trtmt'))
    ->add('tintcode', TextType::class,array('label'=>'Tint_Code'))
    ->add('minmarginrate', TextType::class,array('label'=>'Min_Margin'))
    ->add('maxmarginrate', TextType::class,array('label'=>'Max_Margin'))
     ;
 }

 public function configureOptions(OptionsResolver $resolver)
 {
     $resolver->setDefaults([
         'data_class' => 'AppBundle\Entity\GlassType'
     ]);
 }

}

 ?>
