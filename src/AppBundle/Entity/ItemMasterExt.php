<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PackTypes table
 *
 * @ORM\Table(name="ITEM_MSTR_EXT")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ItemMasterExt
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="ItemMaster", inversedBy="itemnumber")
     * @ORM\Column(name="ITEM_NUMBER",nullable=false)
     */
    private $itemnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="CORP_SKU", type="string", length=9, nullable=true)
     */
    private $corpsku;

    /**
     * @var string
     *
     * @ORM\Column(name="PS_ART_STATUS", type="string", length=9, nullable=true)
     */
    private $psartstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="EXT_MERCH_CAT", type="string", length=9, nullable=true)
     */
    private $extmerchcat;

    /**
     * @var string
     *
     * @ORM\Column(name="UPC", type="string", length=15, nullable=true)
     */
    private $upc;

    /**
     * @var string
     *
     * @ORM\Column(name="CHECKDIG", type="integer", nullable=true)
     */
    private $checkdig;

    /**
     * @var string
     *
     * @ORM\Column(name="CASE_CODE", type="string", length=15, nullable=true)
     */
    private $casecode;



    /**
     * Set the value of Itemnumber
     *
     * @param string itemnumber
     *
     * @return self
     */
    public function setItemnumber($itemnumber)
    {
        $this->itemnumber = $itemnumber;

        return $this;
    }

    /**
     * Get the value of Itemnumber
     *
     * @return string
     */
    public function getItemnumber()
    {
        return $this->itemnumber;
    }

    /**
     * Set the value of Corpsku
     *
     * @param string corpsku
     *
     * @return self
     */
    public function setCorpsku($corpsku)
    {
        $this->corpsku = $corpsku;

        return $this;
    }

    /**
     * Get the value of Corpsku
     *
     * @return string
     */
    public function getCorpsku()
    {
        return $this->corpsku;
    }

    /**
     * Set the value of Psartstatus
     *
     * @param string psartstatus
     *
     * @return self
     */
    public function setPsartstatus($psartstatus)
    {
        $this->psartstatus = $psartstatus;

        return $this;
    }

    /**
     * Get the value of Psartstatus
     *
     * @return string
     */
    public function getPsartstatus()
    {
        return $this->psartstatus;
    }

    /**
     * Set the value of Extmerchcat
     *
     * @param string extmerchcat
     *
     * @return self
     */
    public function setExtmerchcat($extmerchcat)
    {
        $this->extmerchcat = $extmerchcat;

        return $this;
    }

    /**
     * Get the value of Extmerchcat
     *
     * @return string
     */
    public function getExtmerchcat()
    {
        return $this->extmerchcat;
    }

    /**
     * Set the value of Upc
     *
     * @param string upc
     *
     * @return self
     */
    public function setUpc($upc)
    {
        $this->upc = $upc;

        return $this;
    }

    /**
     * Get the value of Upc
     *
     * @return string
     */
    public function getUpc()
    {
        return $this->upc;
    }

    /**
     * Set the value of Checkdig
     *
     * @param string checkdig
     *
     * @return self
     */
    public function setCheckdig($checkdig)
    {
        $this->checkdig = $checkdig;

        return $this;
    }

    /**
     * Get the value of Checkdig
     *
     * @return string
     */
    public function getCheckdig()
    {
        return $this->checkdig;
    }

    /**
     * Set the value of Casecode
     *
     * @param string casecode
     *
     * @return self
     */
    public function setCasecode($casecode)
    {
        $this->casecode = $casecode;

        return $this;
    }

    /**
     * Get the value of Casecode
     *
     * @return string
     */
    public function getCasecode()
    {
        return $this->casecode;
    }

}
