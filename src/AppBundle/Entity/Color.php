<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PackTypes table
 *
 * @ORM\Table(name="IM_TG5181_COLOR")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Color
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="ItemMaster",inversedBy="cteinte")
     * @ORM\Column(name="COLOR_CODE",nullable=false)
     */
    private $colorcode;

    /**
     * @var string
     *
     * @ORM\Column(name="COLOR_FRENCH", type="string", length=25, nullable=true)
     */
    private $colorfrench;

    /**
     * @var string
     *
     * @ORM\Column(name="COLOR_ENGLISH", type="string", length=25, nullable=true)
     */
    private $colorenglish;

    /**
     * @var string
     *
     * @ORM\Column(name="COLOR_ESPN", type="string", length=25, nullable=true)
     */
    private $colorespn;

    /**
     * @var string
     *
     * @ORM\Column(name="PRIMARY_COLOR_INDEX", type="string", length=25, nullable=true)
     */
    private $primarycolorindex;




    /**
     * Set the value of Colorcode
     *
     * @param string colorcode
     *
     * @return self
     */
    public function setColorcode($colorcode)
    {
        $this->colorcode = $colorcode;

        return $this;
    }

    /**
     * Get the value of Colorcode
     *
     * @return string
     */
    public function getColorcode()
    {
        return $this->colorcode;
    }

    /**
     * Set the value of Colorfrench
     *
     * @param string colorfrench
     *
     * @return self
     */
    public function setColorfrench($colorfrench)
    {
        $this->colorfrench = $colorfrench;

        return $this;
    }

    /**
     * Get the value of Colorfrench
     *
     * @return string
     */
    public function getColorfrench()
    {
        return $this->colorfrench;
    }

    /**
     * Set the value of Colorenglish
     *
     * @param string colorenglish
     *
     * @return self
     */
    public function setColorenglish($colorenglish)
    {
        $this->colorenglish = $colorenglish;

        return $this;
    }

    /**
     * Get the value of Colorenglish
     *
     * @return string
     */
    public function getColorenglish()
    {
        return $this->colorenglish;
    }

    /**
     * Set the value of Colorespn
     *
     * @param string colorespn
     *
     * @return self
     */
    public function setColorespn($colorespn)
    {
        $this->colorespn = $colorespn;

        return $this;
    }

    /**
     * Get the value of Colorespn
     *
     * @return string
     */
    public function getColorespn()
    {
        return $this->colorespn;
    }

    /**
     * Set the value of Primarycolorindex
     *
     * @param string primarycolorindex
     *
     * @return self
     */
    public function setPrimarycolorindex($primarycolorindex)
    {
        $this->primarycolorindex = $primarycolorindex;

        return $this;
    }

    /**
     * Get the value of Primarycolorindex
     *
     * @return string
     */
    public function getPrimarycolorindex()
    {
        return $this->primarycolorindex;
    }

}
