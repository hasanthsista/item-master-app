<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Families table
 *
 * @ORM\Table(name="IM_FAMILIES")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Families
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="ItemMaster",inversedBy="collection")
     * @ORM\Column(name="FAMILY",nullable=false)
     */
    private $family;

    /**
     * @var string
     *
     * @ORM\Column(name="USER_CREA", length=20)
     */
    private $userCrea;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE_CREA", type="datetime")
     */
    private $dateCrea;

    /**
     * @var string
     *
     * @ORM\Column(name="USER_MODI", length=20)
     */
    private $userModi;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE_MODI", type="datetime")
     */
    private $dateModi;

    /**
    * @return mixed
    */
    public function getFamily()
    {
      return $this->family;
    }

    /**
    * @return mixed
    */
    public function setFamily($family)
    {
      $this->family = $family;

      return $this;
    }


    /**
    * @return mixed
    */
    public function getUserCrea()
    {
      return $this->userCrea;
    }

    /**
    * @return mixed
    */
    public function setUserCrea($userCrea)
    {
      $this->userCrea = $userCrea;

      return $this;
    }

    /**
    * @return mixed
    */
    public function getDateCrea()
    {
      return $this->dateCrea;
    }

    /**
    * @return mixed
    */
    public function setDateCrea(\DateTime $dateCrea)
    {
      $this->dateCrea = $dateCrea;

      return $this;
    }

    /**
    * @return mixed
    */
    public function getUserModi()
    {
      return $this->userModi;
    }

    /**
    * @return mixed
    */
    public function setUserModi($userModi)
    {
      $this->userModi = $userModi;

      return $this;
    }

    /**
    * @return mixed
    */
    public function getDateModi()
    {
      return $this->dateModi;
    }

    /**
    * @return mixed
    */
    public function setDatemodi($dateModi)
    {
      $this->dateModi = $dateModi;

      return $this;
    }

    public function __construct()
    {
      $this->setDateCrea(new \DateTime());
      if ($this->getDateModi() == null) {
          $this->setDateModi(new \DateTime());
      }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateModifiedDatetime()
    {
      // update the modified time
      $this->setDateModi(new \DateTime());
    }




}
