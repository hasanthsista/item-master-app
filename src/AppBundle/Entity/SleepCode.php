<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PackTypes table
 *
 * @ORM\Table(name="IM_SLEEP_CODES")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class SleepCode
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="ItemMaster",inversedBy="sleepcode")
     * @ORM\Column(name="SLEEP_CODE",nullable=false)
     */
    private $sleepcode;

    /**
     * @var string
     *
     * @ORM\Column(name="DESCRIPTION", type="string", length=25, nullable=true)
     */
    private $description;


    /**
     * @var string
     *
     * @ORM\Column(name="USER_CREA", type="string", length=20, nullable=true)
     */
    private $usercrea;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE_CREA", type="datetime", nullable=true)
     */
    private $datecrea;

    /**
     * @var string
     *
     * @ORM\Column(name="USER_MODI", type="string", length=20, nullable=true)
     */
    private $usermodi;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE_MODI", type="datetime", nullable=true)
     */
    private $datemodi;



    public function __construct()
    {
      $this->setDatecrea(new \DateTime());
      if ($this->getDatemodi() == null) {
          $this->setDatemodi(new \DateTime());
      }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateModifiedDatetime()
    {
      // update the modified time
      $this->setDatemodi(new \DateTime());
    }






    /**
     * Set the value of Description
     *
     * @param string description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Usercrea
     *
     * @param string usercrea
     *
     * @return self
     */
    public function setUsercrea($usercrea)
    {
        $this->usercrea = $usercrea;

        return $this;
    }

    /**
     * Get the value of Usercrea
     *
     * @return string
     */
    public function getUsercrea()
    {
        return $this->usercrea;
    }

    /**
     * Set the value of Datecrea
     *
     * @param \DateTime datecrea
     *
     * @return self
     */
    public function setDatecrea(\DateTime $datecrea)
    {
        $this->datecrea = $datecrea;

        return $this;
    }

    /**
     * Get the value of Datecrea
     *
     * @return \DateTime
     */
    public function getDatecrea()
    {
        return $this->datecrea;
    }

    /**
     * Set the value of Usermodi
     *
     * @param string usermodi
     *
     * @return self
     */
    public function setUsermodi($usermodi)
    {
        $this->usermodi = $usermodi;

        return $this;
    }

    /**
     * Get the value of Usermodi
     *
     * @return string
     */
    public function getUsermodi()
    {
        return $this->usermodi;
    }

    /**
     * Set the value of Datemodi
     *
     * @param \DateTime datemodi
     *
     * @return self
     */
    public function setDatemodi(\DateTime $datemodi)
    {
        $this->datemodi = $datemodi;

        return $this;
    }

    /**
     * Get the value of Datemodi
     *
     * @return \DateTime
     */
    public function getDatemodi()
    {
        return $this->datemodi;
    }


    /**
     * Set the value of Sleepcode
     *
     * @param string sleepcode
     *
     * @return self
     */
    public function setSleepcode($sleepcode)
    {
        $this->sleepcode = $sleepcode;

        return $this;
    }

    /**
     * Get the value of Sleepcode
     *
     * @return string
     */
    public function getSleepcode()
    {
        return $this->sleepcode;
    }

}
