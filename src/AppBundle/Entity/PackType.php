<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PackTypes table
 *
 * @ORM\Table(name="IM_PACK_TYPES")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class PackType
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="ItemMaster",inversedBy="packtype")
     * @ORM\Column(name="PACK_TYPE",nullable=false)
     */
    private $packtype;

    /**
     * @var float
     *
     * @ORM\Column(name="DESCRIPTION", type="string", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="USER_CREA", length=20)
     */
    private $usercrea;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE_CREA", type="datetime")
     */
    private $datecrea;

    /**
     * @var string
     *
     * @ORM\Column(name="USER_MODI", length=20)
     */
    private $usermodi;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE_MODI", type="datetime")
     */
    private $datemodi;

    /**
    * @return mixed
    */
    public function getPackType()
    {
      return $this->packtype;
    }

    /**
    * @return mixed
    */
    public function setPackType($packtype)
    {
      $this->packtype = $packtype;

      return $this;
    }

    /**
    * @return mixed
    */
    public function getDescription()
    {
      return $this->description;
    }

    /**
    * @return mixed
    */
    public function setDescription($description)
    {
      $this->description = $description;

      return $this;
    }

    /**
    * @return mixed
    */
    public function getUsercrea()
    {
      return $this->usercrea;
    }

    /**
    * @return mixed
    */
    public function setUsercrea($usercrea)
    {
      $this->usercrea = $usercrea;

      return $this;
    }

    /**
    * @return mixed
    */
    public function getDatecrea()
    {
      return $this->datecrea;
    }

    /**
    * @return mixed
    */
    public function setDatecrea(\DateTime $datecrea)
    {
      $this->datecrea = $datecrea;

      return $this;
    }

    /**
    * @return mixed
    */
    public function getUsermodi()
    {
      return $this->usermodi;
    }

    /**
    * @return mixed
    */
    public function setUsermodi($usermodi)
    {
      $this->usermodi = $usermodi;

      return $this;
    }

    /**
    * @return mixed
    */
    public function getDatemodi()
    {
      return $this->datemodi;
    }

    /**
    * @return mixed
    */
    public function setDatemodi($datemodi)
    {
      $this->datemodi = $datemodi;

      return $this;
    }

    public function __construct()
    {
      $this->setDatecrea(new \DateTime());
      if ($this->getDatemodi() == null) {
          $this->setDatemodi(new \DateTime());
      }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateModifiedDatetime()
    {
      $this->setDatemodi(new \DateTime());
    }




}
