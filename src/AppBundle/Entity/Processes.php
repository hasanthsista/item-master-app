<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PackTypes table
 *
 * @ORM\Table(name="IM_PROCESSES")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Processes
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="ItemMaster",inversedBy="manprocess")
     * @ORM\Column(name="MAN_PROCESS",nullable=false)
     */
    private $manProcess;

    /**
     * @var string
     *
     * @ORM\Column(name="DESCRIPTION", type="string", length=25, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="MINI")
     */
    private $mini;

    /**
     * @var string
     *
     * @ORM\Column(name="MAXI")
     */
    private $maxi;

    /**
     * @var string
     *
     * @ORM\Column(name="GRLEVEL", type="integer", nullable=true)
     */
    private $grlevel;

    /**
     * @var string
     *
     * @ORM\Column(name="CSECACT", type="integer", nullable=true)
     */
    private $csecact;


    /**
     * @var string
     *
     * @ORM\Column(name="USER_CREA", type="string", length=20, nullable=true)
     */
    private $usercrea;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE_CREA", type="datetime", nullable=true)
     */
    private $datecrea;

    /**
     * @var string
     *
     * @ORM\Column(name="USER_MODI", type="string", length=20, nullable=true)
     */
    private $usermodi;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE_MODI", type="datetime", nullable=true)
     */
    private $datemodi;



    public function __construct()
    {
      $this->setDatecrea(new \DateTime());
      if ($this->getDatemodi() == null) {
          $this->setDatemodi(new \DateTime());
      }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateModifiedDatetime()
    {
      // update the modified time
      $this->setDatemodi(new \DateTime());
    }



    /**
     * Set the value of Man Process
     *
     * @param string manProcess
     *
     * @return self
     */
    public function setManProcess($manProcess)
    {
        $this->manProcess = $manProcess;

        return $this;
    }

    /**
     * Get the value of Man Process
     *
     * @return string
     */
    public function getManProcess()
    {
        return $this->manProcess;
    }

    /**
     * Set the value of Description
     *
     * @param string description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Mini
     *
     * @param string mini
     *
     * @return self
     */
    public function setMini($mini)
    {
        $this->mini = $mini;

        return $this;
    }

    /**
     * Get the value of Mini
     *
     * @return string
     */
    public function getMini()
    {
        return $this->mini;
    }

    /**
     * Set the value of Maxi
     *
     * @param string maxi
     *
     * @return self
     */
    public function setMaxi($maxi)
    {
        $this->maxi = $maxi;

        return $this;
    }

    /**
     * Get the value of Maxi
     *
     * @return string
     */
    public function getMaxi()
    {
        return $this->maxi;
    }

    /**
     * Set the value of Grlevel
     *
     * @param string grlevel
     *
     * @return self
     */
    public function setGrlevel($grlevel)
    {
        $this->grlevel = $grlevel;

        return $this;
    }

    /**
     * Get the value of Grlevel
     *
     * @return string
     */
    public function getGrlevel()
    {
        return $this->grlevel;
    }

    /**
     * Set the value of Csecact
     *
     * @param string csecact
     *
     * @return self
     */
    public function setCsecact($csecact)
    {
        $this->csecact = $csecact;

        return $this;
    }

    /**
     * Get the value of Csecact
     *
     * @return string
     */
    public function getCsecact()
    {
        return $this->csecact;
    }

    /**
     * Set the value of Usercrea
     *
     * @param string usercrea
     *
     * @return self
     */
    public function setUsercrea($usercrea)
    {
        $this->usercrea = $usercrea;

        return $this;
    }

    /**
     * Get the value of Usercrea
     *
     * @return string
     */
    public function getUsercrea()
    {
        return $this->usercrea;
    }

    /**
     * Set the value of Datecrea
     *
     * @param \DateTime datecrea
     *
     * @return self
     */
    public function setDatecrea(\DateTime $datecrea)
    {
        $this->datecrea = $datecrea;

        return $this;
    }

    /**
     * Get the value of Datecrea
     *
     * @return \DateTime
     */
    public function getDatecrea()
    {
        return $this->datecrea;
    }

    /**
     * Set the value of Usermodi
     *
     * @param string usermodi
     *
     * @return self
     */
    public function setUsermodi($usermodi)
    {
        $this->usermodi = $usermodi;

        return $this;
    }

    /**
     * Get the value of Usermodi
     *
     * @return string
     */
    public function getUsermodi()
    {
        return $this->usermodi;
    }

    /**
     * Set the value of Datemodi
     *
     * @param \DateTime datemodi
     *
     * @return self
     */
    public function setDatemodi(\DateTime $datemodi)
    {
        $this->datemodi = $datemodi;

        return $this;
    }

    /**
     * Get the value of Datemodi
     *
     * @return \DateTime
     */
    public function getDatemodi()
    {
        return $this->datemodi;
    }

}
