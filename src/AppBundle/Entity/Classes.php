<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PackTypes table
 *
 * @ORM\Table(name="IM_CLASSES")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Classes
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="ItemMaster",mappedBy="class")
     * @ORM\Column(name="class", nullable=false)
     */
    private $class;

    /**
     * @var string
     *
     * @ORM\Column(name="desclass", type="string",length=50, nullable=true)
     *
     */
    private $desclass;

    /**
     * @var string
     *
     * @ORM\Column(name="line", type="string",length=10, nullable=true)
     *
     */
    private $line;






    /**
     * Set the value of Class
     *
     * @param string class
     *
     * @return self
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Get the value of Class
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set the value of Desclass
     *
     * @param string desclass
     *
     * @return self
     */
    public function setDesclass($desclass)
    {
        $this->desclass = $desclass;

        return $this;
    }

    /**
     * Get the value of Desclass
     *
     * @return string
     */
    public function getDesclass()
    {
        return $this->desclass;
    }

    /**
     * Set the value of Line
     *
     * @param string line
     *
     * @return self
     */
    public function setLine($line)
    {
        $this->line = $line;

        return $this;
    }

    /**
     * Get the value of Line
     *
     * @return string
     */
    public function getLine()
    {
        return $this->line;
    }

}
