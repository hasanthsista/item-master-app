<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Levels table
 *
 * @ORM\Table(name="IM_Levels")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Levels
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="ItemMaster",inversedBy="srlevel")
     * @ORM\Column(name="SR_LEVEL",nullable=false)
     */
    private $srLevel;


        /**
         * @var string
         *
         * @ORM\Column(name="DESCRIPTION", type="string", length=25, nullable=true)
         */
        private $description;

        /**
         * @var integer
         *
         * @ORM\Column(name="RUNMINI", type="integer", nullable=true)
         */
        private $runmini;

        /**
         * @var integer
         *
         * @ORM\Column(name="CHANGEWAIT", type="integer", nullable=true)
         */
        private $changewait;

        /**
         * @var integer
         *
         * @ORM\Column(name="GRLEVEL", type="integer", nullable=true)
         */
        private $grlevel;

        /**
         * @var string
         *
         * @ORM\Column(name="GRDESCRIP", type="string", length=25, nullable=true)
         */
        private $grdescrip;

        /**
         * @var string
         *
         * @ORM\Column(name="USER_CREA", type="string", length=20, nullable=true)
         */
        private $userCrea;

        /**
         * @var \DateTime
         *
         * @ORM\Column(name="DATE_CREA", type="datetime", nullable=true)
         */
        private $dateCrea;

        /**
         * @var string
         *
         * @ORM\Column(name="USER_MODI", type="string", length=20, nullable=true)
         */
        private $userModi;

        /**
         * @var \DateTime
         *
         * @ORM\Column(name="DATE_MODI", type="datetime", nullable=true)
         */
        private $dateModi;

        /**
         * Set srLevel
         *
         * @param string $srLevel
         *
         * @return ImLevels
         */
        public function setSrLevel($srLevel)
        {
            $this->srLevel = $srLevel;

            return $this;
        }

        /**
         * Get srLevel
         *
         * @return integer
         */
        public function getSrLevel()
        {
            return $this->srLevel;
        }





        /**
         * Set description
         *
         * @param string $description
         *
         * @return ImLevels
         */
        public function setDescription($description)
        {
            $this->description = $description;

            return $this;
        }

        /**
         * Get description
         *
         * @return string
         */
        public function getDescription()
        {
            return $this->description;
        }







        /**
         * Set runmini
         *
         * @param integer $runmini
         *
         * @return ImLevels
         */
        public function setRunmini($runmini)
        {
            $this->runmini = $runmini;

            return $this;
        }

        /**
         * Get runmini
         *
         * @return integer
         */
        public function getRunmini()
        {
            return $this->runmini;
        }

        /**
         * Set changewait
         *
         * @param integer $changewait
         *
         * @return ImLevels
         */
        public function setChangewait($changewait)
        {
            $this->changewait = $changewait;

            return $this;
        }

        /**
         * Get changewait
         *
         * @return integer
         */
        public function getChangewait()
        {
            return $this->changewait;
        }

        /**
         * Set grlevel
         *
         * @param integer $grlevel
         *
         * @return ImLevels
         */
        public function setGrlevel($grlevel)
        {
            $this->grlevel = $grlevel;

            return $this;
        }

        /**
         * Get grlevel
         *
         * @return integer
         */
        public function getGrlevel()
        {
            return $this->grlevel;
        }

        /**
         * Set grdescrip
         *
         * @param string $grdescrip
         *
         * @return ImLevels
         */
        public function setGrdescrip($grdescrip)
        {
            $this->grdescrip = $grdescrip;

            return $this;
        }

        /**
         * Get grdescrip
         *
         * @return string
         */
        public function getGrdescrip()
        {
            return $this->grdescrip;
        }

        /**
         * Set userCrea
         *
         * @param string $userCrea
         *
         * @return ImLevels
         */
        public function setUserCrea($userCrea)
        {
            $this->userCrea = $userCrea;

            return $this;
        }

        /**
         * Get userCrea
         *
         * @return string
         */
        public function getUserCrea()
        {
            return $this->userCrea;
        }

        /**
         * Set dateCrea
         *
         * @param \DateTime $dateCrea
         *
         * @return ImLevels
         */
        public function setDateCrea($dateCrea)
        {
            $this->dateCrea = $dateCrea;

            return $this;
        }

        /**
         * Get dateCrea
         *
         * @return \DateTime
         */
        public function getDateCrea()
        {
            return $this->dateCrea;
        }

        /**
         * Set userModi
         *
         * @param string $userModi
         *
         * @return ImLevels
         */
        public function setUserModi($userModi)
        {
            $this->userModi = $userModi;

            return $this;
        }

        /**
         * Get userModi
         *
         * @return string
         */
        public function getUserModi()
        {
            return $this->userModi;
        }

        /**
         * Set dateModi
         *
         * @param \DateTime $dateModi
         *
         * @return ImLevels
         */
        public function setDateModi($dateModi)
        {
            $this->dateModi = $dateModi;

            return $this;
        }

        /**
         * Get dateModi
         *
         * @return \DateTime
         */
        public function getDateModi()
        {
            return $this->dateModi;
        }




        public function __construct()
        {
          $this->setDateCrea(new \DateTime());
          if ($this->getDateModi() == null) {
              $this->setDateModi(new \DateTime());
          }
        }

        /**
         * @ORM\PrePersist()
         * @ORM\PreUpdate()
         */
        public function updateModifiedDatetime()
        {
          // update the modified time
          $this->setDateModi(new \DateTime());
        }



    }
