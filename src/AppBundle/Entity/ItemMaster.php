<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PackTypes table
 *
 * @ORM\Table(name="ITEM_MSTR")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ItemMaster
{


    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="ItemMasterExt", mappedBy="itemnumber")
     * @ORM\Column(name="ITEM_NUMBER")
     */
    private $itemnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="DESCRIPTION", type="string", length=50, nullable=false)
     */
    private $description;


    /**
     * @var string
     *
     * @ORM\Column(name="DESVARIANTE", type="string", length=50, nullable=true)
     */
    private $desvariante;


    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="Decorations",mappedBy="decoration")
     * @ORM\Column(name="DECORATION")
     */
    private $decoration;

    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="UpcCode",mappedBy="upc")
     * @ORM\Column(name="UPC")
     */
    private $upc;

    /**
     * @var string
     *
     * @ORM\Column(name="ITEM_TYPE", type="integer", nullable=true)
     */
    private $itemtype;

    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="PackType",mappedBy="packtype")
     * @ORM\Column(name="PACK_TYPE")
     */
    private $packtype;


    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="Families",mappedBy="family")
     * @ORM\Column(name="COLLECTION")
     */
    private $collection;

    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="Dgform",mappedBy="formdg")
     * @ORM\Column(name="FORM")
     */
    private $form;


    /**
     * @var string
     *
     * @ORM\Column(name="NOMENCLATURE", type="string", length=9, nullable=true)
     */
    private $nomenclature;


    /**
     * @var string
     *
     *
     * @ORM\OneToOne(targetEntity="Processes",mappedBy="manProcess")
     * @ORM\Column(name="MAN_PROCESS")
     */
    private $manprocess;


    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="Levels",mappedBy="srLevel")
     * @ORM\Column(name="SR_LEVEL")
     */
    private $srlevel;


    /**
     * @var string
     *
     * @ORM\Column(name="TEMPERED", type="string", length=9, nullable=true)
     */
    private $tempered;

    /**
     * @var string
     *
     * @ORM\Column(name="NET_WEIGHT", type="decimal", nullable=true)
     */
    private $netweight;

    /**
     * @var string
     *
     * @ORM\Column(name="GROSS_WEIGHT", type="decimal", nullable=true)
     */
    private $grossweight;


    /**
     * @var string
     *
     * @ORM\Column(name="LRGROSS_WT", type="decimal", nullable=true)
     */
    private $lggrosswt;


    /**
     * @var string
     *
     * @ORM\Column(name="ENAMEL", type="decimal", nullable=true)
     */
    private $enamel;

    /**
     * @var string
     *
     * @ORM\Column(name="GOLD", type="decimal", nullable=true)
     */
    private $gold;

    /**
     * @var string
     *
     * @ORM\Column(name="PACKAGING", type="string", length=9, nullable=true)
     */
    private $packaging;


    /**
     * @var string
     *
     * @ORM\Column(name="UNIT_MEASURE", type="integer", nullable=true)
     */
    private $unitmeasure;


    /**
     * @var string
     *
     * @ORM\Column(name="UNIT_PALLET", type="integer", nullable=true)
     */
    private $unitpallet;


    /**
     * @var string
     *
     * @ORM\Column(name="TOTAL_COST", type="decimal", nullable=true)
     */
    private $totalcost;

    /**
     * @var string
     *
     * @ORM\Column(name="SEC_COST", type="decimal", nullable=true)
     */
    private $seccost;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATIONDATE", type="datetime", nullable=true)
     */
    private $creationdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MOD_DATE", type="datetime", nullable=true)
     */
    private $moddate;

    /**
     * @var string
     * @ORM\OneToOne(targetEntity="SleepCode",mappedBy="sleepcode")
     * @ORM\Column(name="SLEEP_CODE")
     */
    private $sleepcode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="SLEEP_DATE", type="datetime", nullable=true)
     */
    private $sleepdate;


    /**
     * @var string
     *
     * @ORM\Column(name="NBMAST", type="integer", nullable=true)
     */
    private $nbmast;

    /**
     * @var string
     *
     * @ORM\Column(name="MINI")
     */
    private $mini;


    /**
     * @var string
     *
     * @ORM\Column(name="MAXI")
     */
    private $maxi;





    /**
     * @var string
     *
     * @ORM\Column(name="MOSALE", type="integer", nullable=true)
     */
    private $mosale;


    /**
     * @var string
     *
     * @ORM\Column(name="LIFEFT", type="integer", nullable=true)
     */
    private $lifeft;

    /**
     * @var string
     *
     * @ORM\Column(name="SUPPLIER", type="string", nullable=true)
     */
    private $supplier;


    /**
     * @var string
     *
     * @ORM\Column(name="NB", type="integer", nullable=true)
     */
    private $nb;


    /**
     * @var string
     *
     * @ORM\Column(name="WIDTH", type="decimal", nullable=true)
     */
    private $width;

    /**
     * @var string
     *
     * @ORM\Column(name="HEIGHT", type="decimal", nullable=true)
     */
    private $height;


    /**
     * @var string
     *
     * @ORM\Column(name="LENGTH", type="decimal", nullable=true)
     */
    private $length;


    /**
     * @var string
     *
     * @ORM\Column(name="CELLS", type="integer", nullable=true)
     */
    private $cells;


    /**
     * @var string
     *
     * @ORM\Column(name="NB2", type="integer", nullable=true)
     */
    private $nb2;


    /**
     * @var string
     *
     * @ORM\Column(name="HEIGHT2", type="decimal", nullable=true)
     */
    private $height2;


    /**
     * @var string
     *
     * @ORM\Column(name="LENGTH2", type="decimal", nullable=true)
     */
    private $length2;


    /**
     * @var string
     *
     * @ORM\Column(name="CELLS2", type="integer", nullable=true)
     */
    private $cells2;


    /**
     * @var string
     *
     * @ORM\Column(name="PACK_QUAL", type="string", length=9, nullable=true)
     */
    private $packqual;


    /**
     * @var string
     *
     * @ORM\Column(name="VCA_NUMBER", type="string",length=12,  nullable=true)
     */
    private $vcanumber;


    /**
     * @var string
     *
     * @ORM\Column(name="MOLD_TYPE", type="string",length=15,  nullable=true)
     */
    private $moldtype;

    /**
     * @var string
     *
     * @ORM\Column(name="NB_BULK", type="integer",  nullable=true)
     */
    private $nbbulk;

    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="Classes",mappedBy="class")
     * @ORM\Column(name="CLASS")
     */
    private $class;

    /**
     * @var string
     *
     * @ORM\Column(name="UFILE", type="string", length=12, nullable=true)
     */
    private $ufile;

    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="GlassType",mappedBy="glasstype")
     * @ORM\Column(name="CTVER")
     */
    private $ctver;


    /**
     * @var string
     * @ORM\OneToOne(targetEntity="Color",mappedBy="colorcode")
     * @ORM\Column(name="CTEINTE")
     */
    private $cteinte;


    /**
     * @var string
     *
     * @ORM\Column(name="CPLAN", type="string", length=9 , nullable=true)
     */
    private $cplan;

    /**
     * @var string
     *
     * @ORM\Column(name="COSTPK", type="decimal", nullable=true)
     */
    private $costpk;


    /**
     * @var string
     *
     * @ORM\Column(name="COSTVCA", type="decimal", nullable=true)
     */
    private $costvca;


    /**
     * @var string
     *
     * @ORM\Column(name="IS_COMMERCIAL", type="integer", nullable=true)
     */
    private $iscommercial;


    /**
     * @var string
     *
     * @ORM\Column(name="LASER", type="string", nullable=true)
     */
    private $laser;


    public function __construct()
    {
      $this->setCreationdate(new \DateTime());
      if ($this->getModdate() == null) {
          $this->setModdate(new \DateTime());
      }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateModifiedDatetime()
    {
      // update the modified time
      $this->setModdate(new \DateTime());
    }






    /**
     * Set the value of Itemnumber
     *
     * @param string itemnumber
     *
     * @return self
     */
    public function setItemnumber($itemnumber)
    {
        $this->itemnumber = $itemnumber;

        return $this;
    }

    /**
     * Get the value of Itemnumber
     *
     * @return string
     */
    public function getItemnumber()
    {
        return $this->itemnumber;
    }

    /**
     * Set the value of Description
     *
     * @param string description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Desvariante
     *
     * @param string desvariante
     *
     * @return self
     */
    public function setDesvariante($desvariante)
    {
        $this->desvariante = $desvariante;

        return $this;
    }

    /**
     * Get the value of Desvariante
     *
     * @return string
     */
    public function getDesvariante()
    {
        return $this->desvariante;
    }

    /**
     * Set the value of Decoration
     *
     * @param string decoration
     *
     * @return self
     */
    public function setDecoration($decoration)
    {
        $this->decoration = $decoration;

        return $this;
    }

    /**
     * Get the value of Decoration
     *
     * @return string
     */
    public function getDecoration()
    {
        return $this->decoration;
    }

    /**
     * Set the value of Upc
     *
     * @param string upc
     *
     * @return self
     */
    public function setUpc($upc)
    {
        $this->upc = $upc;

        return $this;
    }

    /**
     * Get the value of Upc
     *
     * @return string
     */
    public function getUpc()
    {
        return $this->upc;
    }

    /**
     * Set the value of Itemtype
     *
     * @param string itemtype
     *
     * @return self
     */
    public function setItemtype($itemtype)
    {
        $this->itemtype = $itemtype;

        return $this;
    }

    /**
     * Get the value of Itemtype
     *
     * @return string
     */
    public function getItemtype()
    {
        return $this->itemtype;
    }

    /**
     * Set the value of Packtype
     *
     * @param string packtype
     *
     * @return self
     */
    public function setPacktype($packtype)
    {
        $this->packtype = $packtype;

        return $this;
    }

    /**
     * Get the value of Packtype
     *
     * @return string
     */
    public function getPacktype()
    {
        return $this->packtype;
    }

    /**
     * Set the value of Collection
     *
     * @param string collection
     *
     * @return self
     */
    public function setCollection($collection)
    {
        $this->collection = $collection;

        return $this;
    }

    /**
     * Get the value of Collection
     *
     * @return string
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * Set the value of Form
     *
     * @param string form
     *
     * @return self
     */
    public function setForm($form)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Get the value of Form
     *
     * @return string
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Set the value of Nomenclature
     *
     * @param string nomenclature
     *
     * @return self
     */
    public function setNomenclature($nomenclature)
    {
        $this->nomenclature = $nomenclature;

        return $this;
    }

    /**
     * Get the value of Nomenclature
     *
     * @return string
     */
    public function getNomenclature()
    {
        return $this->nomenclature;
    }

    /**
     * Set the value of Manprocess
     *
     * @param string manprocess
     *
     * @return self
     */
    public function setManprocess($manprocess)
    {
        $this->manprocess = $manprocess;

        return $this;
    }

    /**
     * Get the value of Manprocess
     *
     * @return string
     */
    public function getManprocess()
    {
        return $this->manprocess;
    }

    /**
     * Set the value of Srlevel
     *
     * @param string srlevel
     *
     * @return self
     */
    public function setSrlevel($srlevel)
    {
        $this->srlevel = $srlevel;

        return $this;
    }

    /**
     * Get the value of Srlevel
     *
     * @return string
     */
    public function getSrlevel()
    {
        return $this->srlevel;
    }

    /**
     * Set the value of Tempered
     *
     * @param string tempered
     *
     * @return self
     */
    public function setTempered($tempered)
    {
        $this->tempered = $tempered;

        return $this;
    }

    /**
     * Get the value of Tempered
     *
     * @return string
     */
    public function getTempered()
    {
        return $this->tempered;
    }

    /**
     * Set the value of Netweight
     *
     * @param string netweight
     *
     * @return self
     */
    public function setNetweight($netweight)
    {
        $this->netweight = $netweight;

        return $this;
    }

    /**
     * Get the value of Netweight
     *
     * @return string
     */
    public function getNetweight()
    {
        return $this->netweight;
    }

    /**
     * Set the value of Grossweight
     *
     * @param string grossweight
     *
     * @return self
     */
    public function setGrossweight($grossweight)
    {
        $this->grossweight = $grossweight;

        return $this;
    }

    /**
     * Get the value of Grossweight
     *
     * @return string
     */
    public function getGrossweight()
    {
        return $this->grossweight;
    }

    /**
     * Set the value of Lggrosswt
     *
     * @param string lggrosswt
     *
     * @return self
     */
    public function setLggrosswt($lggrosswt)
    {
        $this->lggrosswt = $lggrosswt;

        return $this;
    }

    /**
     * Get the value of Lggrosswt
     *
     * @return string
     */
    public function getLggrosswt()
    {
        return $this->lggrosswt;
    }

    /**
     * Set the value of Enamel
     *
     * @param string enamel
     *
     * @return self
     */
    public function setEnamel($enamel)
    {
        $this->enamel = $enamel;

        return $this;
    }

    /**
     * Get the value of Enamel
     *
     * @return string
     */
    public function getEnamel()
    {
        return $this->enamel;
    }

    /**
     * Set the value of Gold
     *
     * @param string gold
     *
     * @return self
     */
    public function setGold($gold)
    {
        $this->gold = $gold;

        return $this;
    }

    /**
     * Get the value of Gold
     *
     * @return string
     */
    public function getGold()
    {
        return $this->gold;
    }

    /**
     * Set the value of Packaging
     *
     * @param string packaging
     *
     * @return self
     */
    public function setPackaging($packaging)
    {
        $this->packaging = $packaging;

        return $this;
    }

    /**
     * Get the value of Packaging
     *
     * @return string
     */
    public function getPackaging()
    {
        return $this->packaging;
    }

    /**
     * Set the value of Unitmeasure
     *
     * @param string unitmeasure
     *
     * @return self
     */
    public function setUnitmeasure($unitmeasure)
    {
        $this->unitmeasure = $unitmeasure;

        return $this;
    }

    /**
     * Get the value of Unitmeasure
     *
     * @return string
     */
    public function getUnitmeasure()
    {
        return $this->unitmeasure;
    }

    /**
     * Set the value of Unitpallet
     *
     * @param string unitpallet
     *
     * @return self
     */
    public function setUnitpallet($unitpallet)
    {
        $this->unitpallet = $unitpallet;

        return $this;
    }

    /**
     * Get the value of Unitpallet
     *
     * @return string
     */
    public function getUnitpallet()
    {
        return $this->unitpallet;
    }

    /**
     * Set the value of Totalcost
     *
     * @param string totalcost
     *
     * @return self
     */
    public function setTotalcost($totalcost)
    {
        $this->totalcost = $totalcost;

        return $this;
    }

    /**
     * Get the value of Totalcost
     *
     * @return string
     */
    public function getTotalcost()
    {
        return $this->totalcost;
    }

    /**
     * Set the value of Seccost
     *
     * @param string seccost
     *
     * @return self
     */
    public function setSeccost($seccost)
    {
        $this->seccost = $seccost;

        return $this;
    }

    /**
     * Get the value of Seccost
     *
     * @return string
     */
    public function getSeccost()
    {
        return $this->seccost;
    }

    /**
     * Set the value of Creationdate
     *
     * @param \DateTime creationdate
     *
     * @return self
     */
    public function setCreationdate(\DateTime $creationdate)
    {
        $this->creationdate = $creationdate;

        return $this;
    }

    /**
     * Get the value of Creationdate
     *
     * @return \DateTime
     */
    public function getCreationdate()
    {
        return $this->creationdate;
    }

    /**
     * Set the value of Moddate
     *
     * @param \DateTime moddate
     *
     * @return self
     */
    public function setModdate(\DateTime $moddate)
    {
        $this->moddate = $moddate;

        return $this;
    }

    /**
     * Get the value of Moddate
     *
     * @return \DateTime
     */
    public function getModdate()
    {
        return $this->moddate;
    }

    /**
     * Set the value of Sleepcode
     *
     * @param string sleepcode
     *
     * @return self
     */
    public function setSleepcode($sleepcode)
    {
        $this->sleepcode = $sleepcode;

        return $this;
    }

    /**
     * Get the value of Sleepcode
     *
     * @return string
     */
    public function getSleepcode()
    {
        return $this->sleepcode;
    }

    /**
     * Set the value of Sleepdate
     *
     * @param \DateTime sleepdate
     *
     * @return self
     */
    public function setSleepdate(\DateTime $sleepdate)
    {
        $this->sleepdate = $sleepdate;

        return $this;
    }

    /**
     * Get the value of Sleepdate
     *
     * @return \DateTime
     */
    public function getSleepdate()
    {
        return $this->sleepdate;
    }

    /**
     * Set the value of Nbmast
     *
     * @param string nbmast
     *
     * @return self
     */
    public function setNbmast($nbmast)
    {
        $this->nbmast = $nbmast;

        return $this;
    }

    /**
     * Get the value of Nbmast
     *
     * @return string
     */
    public function getNbmast()
    {
        return $this->nbmast;
    }

    /**
     * Set the value of Mini
     *
     * @param string mini
     *
     * @return self
     */
    public function setMini($mini)
    {
        $this->mini = $mini;

        return $this;
    }

    /**
     * Get the value of Mini
     *
     * @return string
     */
    public function getMini()
    {
        return $this->mini;
    }

    /**
     * Set the value of Maxi
     *
     * @param string maxi
     *
     * @return self
     */
    public function setMaxi($maxi)
    {
        $this->maxi = $maxi;

        return $this;
    }

    /**
     * Get the value of Maxi
     *
     * @return string
     */
    public function getMaxi()
    {
        return $this->maxi;
    }

    /**
     * Set the value of Mosale
     *
     * @param string mosale
     *
     * @return self
     */
    public function setMosale($mosale)
    {
        $this->mosale = $mosale;

        return $this;
    }

    /**
     * Get the value of Mosale
     *
     * @return string
     */
    public function getMosale()
    {
        return $this->mosale;
    }

    /**
     * Set the value of Lifeft
     *
     * @param string lifeft
     *
     * @return self
     */
    public function setLifeft($lifeft)
    {
        $this->lifeft = $lifeft;

        return $this;
    }

    /**
     * Get the value of Lifeft
     *
     * @return string
     */
    public function getLifeft()
    {
        return $this->lifeft;
    }

    /**
     * Set the value of Supplier
     *
     * @param string supplier
     *
     * @return self
     */
    public function setSupplier($supplier)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get the value of Supplier
     *
     * @return string
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Set the value of Nb
     *
     * @param string nb
     *
     * @return self
     */
    public function setNb($nb)
    {
        $this->nb = $nb;

        return $this;
    }

    /**
     * Get the value of Nb
     *
     * @return string
     */
    public function getNb()
    {
        return $this->nb;
    }

    /**
     * Set the value of Width
     *
     * @param string width
     *
     * @return self
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get the value of Width
     *
     * @return string
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set the value of Height
     *
     * @param string height
     *
     * @return self
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get the value of Height
     *
     * @return string
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set the value of Length
     *
     * @param string length
     *
     * @return self
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get the value of Length
     *
     * @return string
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set the value of Cells
     *
     * @param string cells
     *
     * @return self
     */
    public function setCells($cells)
    {
        $this->cells = $cells;

        return $this;
    }

    /**
     * Get the value of Cells
     *
     * @return string
     */
    public function getCells()
    {
        return $this->cells;
    }

    /**
     * Set the value of Nb
     *
     * @param string nb2
     *
     * @return self
     */
    public function setNb2($nb2)
    {
        $this->nb2 = $nb2;

        return $this;
    }

    /**
     * Get the value of Nb
     *
     * @return string
     */
    public function getNb2()
    {
        return $this->nb2;
    }

    /**
     * Set the value of Height
     *
     * @param string height2
     *
     * @return self
     */
    public function setHeight2($height2)
    {
        $this->height2 = $height2;

        return $this;
    }

    /**
     * Get the value of Height
     *
     * @return string
     */
    public function getHeight2()
    {
        return $this->height2;
    }

    /**
     * Set the value of Length
     *
     * @param string length2
     *
     * @return self
     */
    public function setLength2($length2)
    {
        $this->length2 = $length2;

        return $this;
    }

    /**
     * Get the value of Length
     *
     * @return string
     */
    public function getLength2()
    {
        return $this->length2;
    }

    /**
     * Set the value of Cells
     *
     * @param string cells2
     *
     * @return self
     */
    public function setCells2($cells2)
    {
        $this->cells2 = $cells2;

        return $this;
    }

    /**
     * Get the value of Cells
     *
     * @return string
     */
    public function getCells2()
    {
        return $this->cells2;
    }

    /**
     * Set the value of Packqual
     *
     * @param string packqual
     *
     * @return self
     */
    public function setPackqual($packqual)
    {
        $this->packqual = $packqual;

        return $this;
    }

    /**
     * Get the value of Packqual
     *
     * @return string
     */
    public function getPackqual()
    {
        return $this->packqual;
    }

    /**
     * Set the value of Vcanumber
     *
     * @param string vcanumber
     *
     * @return self
     */
    public function setVcanumber($vcanumber)
    {
        $this->vcanumber = $vcanumber;

        return $this;
    }

    /**
     * Get the value of Vcanumber
     *
     * @return string
     */
    public function getVcanumber()
    {
        return $this->vcanumber;
    }

    /**
     * Set the value of Moldtype
     *
     * @param string moldtype
     *
     * @return self
     */
    public function setMoldtype($moldtype)
    {
        $this->moldtype = $moldtype;

        return $this;
    }

    /**
     * Get the value of Moldtype
     *
     * @return string
     */
    public function getMoldtype()
    {
        return $this->moldtype;
    }

    /**
     * Set the value of Nbbulk
     *
     * @param string nbbulk
     *
     * @return self
     */
    public function setNbbulk($nbbulk)
    {
        $this->nbbulk = $nbbulk;

        return $this;
    }

    /**
     * Get the value of Nbbulk
     *
     * @return string
     */
    public function getNbbulk()
    {
        return $this->nbbulk;
    }

    /**
     * Set the value of Class
     *
     * @param string class
     *
     * @return self
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Get the value of Class
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set the value of Ufile
     *
     * @param string ufile
     *
     * @return self
     */
    public function setUfile($ufile)
    {
        $this->ufile = $ufile;

        return $this;
    }

    /**
     * Get the value of Ufile
     *
     * @return string
     */
    public function getUfile()
    {
        return $this->ufile;
    }

    /**
     * Set the value of Ctver
     *
     * @param string ctver
     *
     * @return self
     */
    public function setCtver($ctver)
    {
        $this->ctver = $ctver;

        return $this;
    }

    /**
     * Get the value of Ctver
     *
     * @return string
     */
    public function getCtver()
    {
        return $this->ctver;
    }

    /**
     * Set the value of Cteinte
     *
     * @param string cteinte
     *
     * @return self
     */
    public function setCteinte($cteinte)
    {
        $this->cteinte = $cteinte;

        return $this;
    }

    /**
     * Get the value of Cteinte
     *
     * @return string
     */
    public function getCteinte()
    {
        return $this->cteinte;
    }

    /**
     * Set the value of Cplan
     *
     * @param string cplan
     *
     * @return self
     */
    public function setCplan($cplan)
    {
        $this->cplan = $cplan;

        return $this;
    }

    /**
     * Get the value of Cplan
     *
     * @return string
     */
    public function getCplan()
    {
        return $this->cplan;
    }

    /**
     * Set the value of Costpk
     *
     * @param string costpk
     *
     * @return self
     */
    public function setCostpk($costpk)
    {
        $this->costpk = $costpk;

        return $this;
    }

    /**
     * Get the value of Costpk
     *
     * @return string
     */
    public function getCostpk()
    {
        return $this->costpk;
    }

    /**
     * Set the value of Costvca
     *
     * @param string costvca
     *
     * @return self
     */
    public function setCostvca($costvca)
    {
        $this->costvca = $costvca;

        return $this;
    }

    /**
     * Get the value of Costvca
     *
     * @return string
     */
    public function getCostvca()
    {
        return $this->costvca;
    }

    /**
     * Set the value of Iscommercial
     *
     * @param string iscommercial
     *
     * @return self
     */
    public function setIscommercial($iscommercial)
    {
        $this->iscommercial = $iscommercial;

        return $this;
    }

    /**
     * Get the value of Iscommercial
     *
     * @return string
     */
    public function getIscommercial()
    {
        return $this->iscommercial;
    }

    /**
     * Set the value of Laser
     *
     * @param string laser
     *
     * @return self
     */
    public function setLaser($laser)
    {
        $this->laser = $laser;

        return $this;
    }

    /**
     * Get the value of Laser
     *
     * @return string
     */
    public function getLaser()
    {
        return $this->laser;
    }

}
