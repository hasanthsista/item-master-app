<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DGFORM table
 *
 * @ORM\Table(name="IM_DGFORM")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Dgform
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="ItemMaster",inversedBy="form")
     * @ORM\Column(name="FORM",nullable=false)
     */
    private $formdg;

    /**
     * @var string
     *
     * @ORM\Column(name="DESCRIPTION", type="string", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="USER_CREA", length=20)
     */
    private $usercrea;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE_CREA", type="datetime")
     */
    private $datecrea;

    /**
     * @var string
     *
     * @ORM\Column(name="USER_MODI", length=20)
     */
    private $usermodi;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE_MODI", type="datetime")
     */
    private $datemodi;




    /**
    * @return mixed
    */
    public function getFormdg()
    {
      return $this->formdg;
    }

    /**
    * @return mixed
    */
    public function setFormdg($formdg)
    {
      $this->formdg = $formdg;

      return $this;
    }

    /**
    * @return mixed
    */
    public function getDescription()
    {
      return $this->description;
    }

    /**
    * @return mixed
    */
    public function setDescription($description)
    {
      $this->description = $description;

      return $this;
    }

    /**
    * @return mixed
    */
    public function getUsercrea()
    {
      return $this->usercrea;
    }

    /**
    * @return mixed
    */
    public function setUsercrea($usercrea)
    {
      $this->usercrea = $usercrea;

      return $this;
    }

    /**
    * @return mixed
    */
    public function getDatecrea()
    {
      return $this->datecrea;
    }

    /**
    * @return mixed
    */
    public function setDatecrea(\DateTime $datecrea)
    {
      $this->datecrea = $datecrea;

      return $this;
    }

    /**
    * @return mixed
    */
    public function getUsermodi()
    {
      return $this->usermodi;
    }

    /**
    * @return mixed
    */
    public function setUsermodi($usermodi)
    {
      $this->usermodi = $usermodi;

      return $this;
    }

    /**
    * @return mixed
    */
    public function getDatemodi()
    {
      return $this->datemodi;
    }

    /**
    * @return mixed
    */
    public function setDatemodi($datemodi)
    {
      $this->datemodi = $datemodi;

      return $this;
    }

    public function __construct()
    {
      $this->setDatecrea(new \DateTime());
      if ($this->getDatemodi() == null) {
          $this->setDatemodi(new \DateTime());
      }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateModifiedDatetime()
    {
      // update the modified time
      $this->setDatemodi(new \DateTime());
    }




}
