<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Decorations table
 *
 * @ORM\Table(name="IM_DECORATIONS")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Decorations
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="ItemMaster",inversedBy="decoration")
     * @ORM\Column(name="DECORATION",nullable=false)
     */
    private $decoration;

    /**
     * @var float
     *
     * @ORM\Column(name="SCREENS", type="integer", nullable=true)
     */
    private $screens;

    /**
     * @var string
     *
     * @ORM\Column(name="USER_CREA", length=20)
     */
    private $usercrea;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE_CREA", type="datetime")
     */
    private $datecrea;

    /**
     * @var string
     *
     * @ORM\Column(name="USER_MODI", length=20)
     */
    private $usermodi;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE_MODI", type="datetime")
     */
    private $datemodi;

    /**
    * @return mixed
    */
    public function getDecoration()
    {
      return $this->decoration;
    }

    /**
    * @return mixed
    */
    public function setDecoration($decoration)
    {
      $this->decoration = $decoration;

      return $this;
    }

    /**
    * @return mixed
    */
    public function getScreens()
    {
      return $this->screens;
    }

    /**
    * @return mixed
    */
    public function setScreens($screens)
    {
      $this->screens = $screens;

      return $this;
    }

    /**
    * @return mixed
    */
    public function getUsercrea()
    {
      return $this->usercrea;
    }

    /**
    * @return mixed
    */
    public function setUsercrea($usercrea)
    {
      $this->usercrea = $usercrea;

      return $this;
    }

    /**
    * @return mixed
    */
    public function getDatecrea()
    {
      return $this->datecrea;
    }

    /**
    * @return mixed
    */
    public function setDatecrea(\DateTime $datecrea)
    {
      $this->datecrea = $datecrea;

      return $this;
    }

    /**
    * @return mixed
    */
    public function getUsermodi()
    {
      return $this->usermodi;
    }

    /**
    * @return mixed
    */
    public function setUsermodi($usermodi)
    {
      $this->usermodi = $usermodi;

      return $this;
    }

    /**
    * @return mixed
    */
    public function getDatemodi()
    {
      return $this->datemodi;
    }

    /**
    * @return mixed
    */
    public function setDatemodi($datemodi)
    {
      $this->datemodi = $datemodi;

      return $this;
    }

    public function __construct()
    {
      $this->setDatecrea(new \DateTime());
      if ($this->getDatemodi() == null) {
          $this->setDatemodi(new \DateTime());
      }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateModifiedDatetime()
    {
      $this->setDatemodi(new \DateTime());
    }



}
