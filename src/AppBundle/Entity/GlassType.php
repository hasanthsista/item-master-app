<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PackTypes table
 *
 * @ORM\Table(name="IM_TG5180_GLASSTYPE")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class GlassType
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="ItemMaster",inversedBy="ctver")
     * @ORM\Column(name="GLASS_TYPE_CODE",nullable=false)
     */
    private $glasstype;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE_FRENCH", type="string", length=25, nullable=true)
     */
    private $frenchtype;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE_ENGLISH", type="string", length=25, nullable=true)
     */
    private $englishtype;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE_ESPN", type="string", length=25, nullable=true)
     */
    private $espntype;

    /**
     * @var string
     *
     * @ORM\Column(name="HEAT_TRTMT_CODE", type="string", length=2, nullable=true)
     */
    private $heattrtmtcode;

    /**
     * @var string
     *
     * @ORM\Column(name="COLD_TRTMT_CODE", type="string", length=2, nullable=true)
     */
    private $coldtrtmtcode;

    /**
     * @var string
     *
     * @ORM\Column(name="TINT_CODE", type="string", length=2, nullable=true)
     */
    private $tintcode;

    /**
     * @var string
     *
     * @ORM\Column(name="MIN_MARGIN_RATE", type="float", nullable=true)
     */
    private $minmarginrate;

    /**
     * @var string
     *
     * @ORM\Column(name="MAX_MARGIN_RATE", type="float", nullable=true)
     */
    private $maxmarginrate;




    /**
     * Set the value of Glasstype
     *
     * @param string glasstype
     *
     * @return self
     */
    public function setGlasstype($glasstype)
    {
        $this->glasstype = $glasstype;

        return $this;
    }

    /**
     * Get the value of Glasstype
     *
     * @return string
     */
    public function getGlasstype()
    {
        return $this->glasstype;
    }

    /**
     * Set the value of Frenchtype
     *
     * @param string frenchtype
     *
     * @return self
     */
    public function setFrenchtype($frenchtype)
    {
        $this->frenchtype = $frenchtype;

        return $this;
    }

    /**
     * Get the value of Frenchtype
     *
     * @return string
     */
    public function getFrenchtype()
    {
        return $this->frenchtype;
    }

    /**
     * Set the value of Englishtype
     *
     * @param string englishtype
     *
     * @return self
     */
    public function setEnglishtype($englishtype)
    {
        $this->englishtype = $englishtype;

        return $this;
    }

    /**
     * Get the value of Englishtype
     *
     * @return string
     */
    public function getEnglishtype()
    {
        return $this->englishtype;
    }

    /**
     * Set the value of Espntype
     *
     * @param string espntype
     *
     * @return self
     */
    public function setEspntype($espntype)
    {
        $this->espntype = $espntype;

        return $this;
    }

    /**
     * Get the value of Espntype
     *
     * @return string
     */
    public function getEspntype()
    {
        return $this->espntype;
    }

    /**
     * Set the value of Heattrtmtcode
     *
     * @param string heattrtmtcode
     *
     * @return self
     */
    public function setHeattrtmtcode($heattrtmtcode)
    {
        $this->heattrtmtcode = $heattrtmtcode;

        return $this;
    }

    /**
     * Get the value of Heattrtmtcode
     *
     * @return string
     */
    public function getHeattrtmtcode()
    {
        return $this->heattrtmtcode;
    }

    /**
     * Set the value of Coldtrtmtcode
     *
     * @param string coldtrtmtcode
     *
     * @return self
     */
    public function setColdtrtmtcode($coldtrtmtcode)
    {
        $this->coldtrtmtcode = $coldtrtmtcode;

        return $this;
    }

    /**
     * Get the value of Coldtrtmtcode
     *
     * @return string
     */
    public function getColdtrtmtcode()
    {
        return $this->coldtrtmtcode;
    }

    /**
     * Set the value of Tintcode
     *
     * @param string tintcode
     *
     * @return self
     */
    public function setTintcode($tintcode)
    {
        $this->tintcode = $tintcode;

        return $this;
    }

    /**
     * Get the value of Tintcode
     *
     * @return string
     */
    public function getTintcode()
    {
        return $this->tintcode;
    }

    /**
     * Set the value of Minmarginrate
     *
     * @param string minmarginrate
     *
     * @return self
     */
    public function setMinmarginrate($minmarginrate)
    {
        $this->minmarginrate = $minmarginrate;

        return $this;
    }

    /**
     * Get the value of Minmarginrate
     *
     * @return string
     */
    public function getMinmarginrate()
    {
        return $this->minmarginrate;
    }

    /**
     * Set the value of Maxmarginrate
     *
     * @param string maxmarginrate
     *
     * @return self
     */
    public function setMaxmarginrate($maxmarginrate)
    {
        $this->maxmarginrate = $maxmarginrate;

        return $this;
    }

    /**
     * Get the value of Maxmarginrate
     *
     * @return string
     */
    public function getMaxmarginrate()
    {
        return $this->maxmarginrate;
    }

}
